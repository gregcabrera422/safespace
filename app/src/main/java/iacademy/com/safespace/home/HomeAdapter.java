package iacademy.com.safespace.home;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.create.CreateActivity;
import iacademy.com.safespace.track.TrackActivity;
import iacademy.com.safespace.know.KnowActivity;
import iacademy.com.safespace.meditate.MeditateActivity;
import iacademy.com.safespace.model.HomeItemModel;
import iacademy.com.safespace.utility.FontHelper;

public class HomeAdapter extends PagerAdapter {

    @BindView(R.id.image_layout)
    RelativeLayout logoImageView;

    @BindView(R.id.title_textview)
    TextView titleTextView;

    @BindView(R.id.description_textview)
    TextView descriptionTextView;

    private LayoutInflater layoutInflater;

    private Context context;

    private List<HomeItemModel> homeItemModelList;

    public HomeAdapter(Context context) {
        this.context = context;
        homeItemModelList = new ArrayList<>();

        homeItemModelList.add(new HomeItemModel("track", "Jot down thoughts to ease the mind.", R.drawable.track_btn));
        homeItemModelList.add(new HomeItemModel("create", "Visualize your thoughts and emotions.", R.drawable.create_btn));
        homeItemModelList.add(new HomeItemModel("meditate", "Take a break and relax!", R.drawable.meditate_btn));
        homeItemModelList.add(new HomeItemModel("know", "Be informed and get help.", R.drawable.know_btn));
    }

    @Override
    public int getCount() {
        return homeItemModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.home_item_view, container, false);
        ButterKnife.bind(this, view);
        titleTextView.setTypeface(FontHelper.BOLD(context));
        descriptionTextView.setTypeface(FontHelper.BOLD(context));
        logoImageView.setBackgroundResource(homeItemModelList.get(position).getImage());
        titleTextView.setText(homeItemModelList.get(position).getTitle());
        descriptionTextView.setText(homeItemModelList.get(position).getDescription());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(homeItemModelList.get(position).getTitle()){
                    case "track":
                        context.startActivity(new Intent(context, TrackActivity.class));
                        break;
                    case "create":
                        context.startActivity(new Intent(context, CreateActivity.class));
                        break;
                    case "meditate":
                        context.startActivity(new Intent(context, MeditateActivity.class));
                        break;
                    case "know":
                        context.startActivity(new Intent(context, KnowActivity.class));
                        break;
                }
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @OnClick
    void onClick(View view){
        System.out.println();
    }

}
