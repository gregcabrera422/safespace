package iacademy.com.safespace.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

public class AvatarPartsModel implements Parcelable {
    private int image;
    private int x;
    private int y;
    private String color;

    public AvatarPartsModel(int image, int x, int y, String color){
        this.image = image;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    protected AvatarPartsModel(Parcel in) {
        image = in.readInt();
        x = in.readInt();
        y = in.readInt();
        color = in.readString();
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) { this.image = image; }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Bitmap getBitmap(Context context){
        return BitmapFactory.decodeResource(context.getResources(), this.image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeString(color);
    }

    public static final Creator<AvatarPartsModel> CREATOR = new Creator<AvatarPartsModel>() {
        @Override
        public AvatarPartsModel createFromParcel(Parcel in) {
            return new AvatarPartsModel(in);
        }

        @Override
        public AvatarPartsModel[] newArray(int size) {
            return new AvatarPartsModel[size];
        }
    };
}
