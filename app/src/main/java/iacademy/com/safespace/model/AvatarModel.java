package iacademy.com.safespace.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

import java.util.ArrayList;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.Assets;
import static iacademy.com.safespace.utility.Assets.filterSet1;
import static iacademy.com.safespace.utility.Assets.filterSet2;
import static iacademy.com.safespace.utility.ImageHelper.getResizedBitmap;

public class AvatarModel {
    private AvatarPartsModel eyes;
    private AvatarPartsModel skin;
    private AvatarPartsModel nose;
    private AvatarPartsModel hair;
    private AvatarPartsModel shirt;
    private AvatarPartsModel lips;
    private AvatarPartsModel frame;
    private Context context;

    public AvatarModel(AvatarPartsModel eyes, AvatarPartsModel skin, AvatarPartsModel nose, AvatarPartsModel hair, AvatarPartsModel shirt, AvatarPartsModel lips, Context context){
        this.eyes = eyes;
        this.skin = skin;
        this.nose = nose;
        this.hair = hair;
        this.shirt = shirt;
        this.lips = lips;
        this.context = context;
        this.frame = Assets.getFrame();
    }

    public AvatarModel(Context context){
        this.context = context;
        this.frame = Assets.getFrame();
    }

    public AvatarPartsModel getEyes() {
        return eyes;
    }

    public void setEyes(AvatarPartsModel eyes) {
        this.eyes = eyes;
    }

    public AvatarPartsModel getSkin() {
        return skin;
    }

    public void setSkin(AvatarPartsModel skin) {
        this.skin = skin;
    }

    public AvatarPartsModel getNose() {
        return nose;
    }

    public void setNose(AvatarPartsModel nose) {
        this.nose = nose;
    }

    public AvatarPartsModel getHair() {
        return hair;
    }

    public void setHair(AvatarPartsModel hair) {
        this.hair = hair;
    }

    public AvatarPartsModel getShirt() {
        return shirt;
    }

    public void setShirt(AvatarPartsModel shirt) {
        this.shirt = shirt;
    }

    public AvatarPartsModel getLips() {
        return lips;
    }

    public void setLips(AvatarPartsModel lips) {
        this.lips = lips;
    }

    public Bitmap generateAvatar(){
        Bitmap avatar = Bitmap.createBitmap(scaleX(500), scaleY(500), skin.getBitmap(context).getConfig());

        Canvas canvas = new Canvas(avatar);
        canvas.drawBitmap(frame.getBitmap(context), scaleX(frame.getX()), scaleY(frame.getY()), null);
        canvas.drawBitmap(skin.getBitmap(context), scaleX(skin.getX()), scaleY(skin.getY()), null);
        canvas.drawBitmap(shirt.getBitmap(context), scaleX(shirt.getX()), scaleY(shirt.getY()), null);
        canvas.drawBitmap(eyes.getBitmap(context), scaleX(eyes.getX()), scaleY(eyes.getY()), null);
        canvas.drawBitmap(hair.getBitmap(context), scaleX(hair.getX()), scaleY(hair.getY()), null);
        canvas.drawBitmap(nose.getBitmap(context), scaleX(nose.getX()), scaleY(nose.getY()), null);
        canvas.drawBitmap(lips.getBitmap(context), scaleX(lips.getX()), scaleY(lips.getY()), null);

        return getResizedBitmap(avatar, 800, 800);
    }

    public void updateAvatar(int pointer, int id, String tag){
        ArrayList<AvatarPartsModel> asset = null;
        switch (pointer){
            case 0:
                asset = Assets.getSkins();
                setSkin(getAsset(asset, id, tag));
                break;
            case 1:
                asset = Assets.getShirts();
                setShirt(getAsset(asset, id, tag));
                break;
            case 2:
                asset = Assets.getHair();
                setHair(getAsset(asset, id, tag));
                break;
            case 3:
                asset = Assets.getEyes();
                setEyes(getAsset(asset, id, tag));
                break;
            case 4:
                asset = Assets.getNose();
                setNose(getAsset(asset, id, tag));
                break;
            case 5:
                asset = Assets.getLips();
                setLips(getAsset(asset, id, tag));
                break;
        }


    }

    private AvatarPartsModel getAsset(ArrayList<AvatarPartsModel> asset, int id, String tag){
        AvatarPartsModel part = null;
        if (tag.equals("brown")){
            asset = filterSet2(asset);
        }else{
            asset = filterSet1(asset);
        }

        switch (id){
            case R.id.layout1:
                part = asset.get(0);
                break;
            case R.id.layout2:
                part = asset.get(1);
                break;
            case R.id.layout3:
                part = asset.get(2);
                break;
            case R.id.layout4:
                part = asset.get(3);
                break;
            case R.id.layout5:
                part = asset.get(4);
                break;
            case R.id.layout6:
                part = asset.get(5);
                break;
        }
        return part;
    }

    public ArrayList<AvatarPartsModel> getSavable(){
        ArrayList<AvatarPartsModel>parts = new ArrayList<>();
        parts.add(eyes);
        parts.add(skin);
        parts.add(nose);
        parts.add(hair);
        parts.add(shirt);
        parts.add(lips);
        return parts;
    }

    public void parseArrayList(ArrayList<AvatarPartsModel> parts){
        setEyes(parts.get(0));
        setSkin(parts.get(1));
        setNose(parts.get(2));
        setHair(parts.get(3));
        setShirt(parts.get(4));
        setLips(parts.get(5));
    }

    public int scaleX(int dp) {
        return Math.round(dp * context.getResources().getDisplayMetrics().density);
    }

    public int scaleY(int dp) {
        return Math.round(dp * context.getResources().getDisplayMetrics().density);
    }
}
