package iacademy.com.safespace.model;

import android.graphics.Bitmap;

public class ArtworkModel {

    private Bitmap artWorkBitmap;
    private String fileName;

    public ArtworkModel(Bitmap artWorkBitmap, String fileName) {
        this.artWorkBitmap = artWorkBitmap;
        this.fileName = fileName;
    }

    public Bitmap getArtWorkBitmap() {
        return artWorkBitmap;
    }

    public void setArtWorkBitmap(Bitmap artWorkBitmap) {
        this.artWorkBitmap = artWorkBitmap;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
