package iacademy.com.safespace.model;

public class MoodModel {
    private String though;
    private String task;
    private String mood;
    private int task_img;

    public String getThough() {
        return though;
    }

    public void setThough(String though) {
        this.though = though;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public int getTask_img() {
        return task_img;
    }

    public void setTask_img(int task_img) {
        this.task_img = task_img;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }
}
