package iacademy.com.safespace.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import iacademy.com.safespace.R;
import iacademy.com.safespace.moodcheckin.CheckInHomeActivity;
import iacademy.com.safespace.moodcheckin.FeelingBetterActivity;
import iacademy.com.safespace.track.MoodActivity;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

public class SavePaintDialog extends Dialog {

    @BindView(R.id.paint_image_view)
    CircleImageView paintIv;

    @BindView(R.id.paint_name_edit_text)
    EditText paintNameEt;

    Context context;

    Callback callback;

    String fileName;

    public SavePaintDialog(Context context, Callback callback) {
        super(context);
        this.context = context;
        this.callback = callback;

        //makes background transparent
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_save_dialog);
        ButterKnife.bind(this);
    }

    public void show(Bitmap cacheBitmap) {
        show();
        paintIv.setImageBitmap(cacheBitmap);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @OnClick(R.id.yes_btn)
    public void yesClicked(){
        if(TextUtils.isEmpty(paintNameEt.getText().toString().trim())){
            paintNameEt.setError("Please enter artwork name.");
        } else {
            setFileName(paintNameEt.getText().toString());
            callback.yesClicked();

            boolean isSkipClicked = new SharedPreferencesHelper((Activity) context).getSharedPrefs.getBoolean("isSkipClicked", false);
            if (!isSkipClicked){
                new SharedPreferencesHelper((Activity) context).Write("isSkipClicked", true);
                Intent intent = new Intent(context, FeelingBetterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        }
    }

    @OnClick(R.id.no_btn)
    public void noClicked(){
        callback.noClicked();
    }

    public interface Callback {
        void yesClicked();
        void noClicked();
    }
}
