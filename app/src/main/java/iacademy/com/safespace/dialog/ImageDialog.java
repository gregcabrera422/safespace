package iacademy.com.safespace.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import iacademy.com.safespace.R;

public class ImageDialog extends Dialog {

    Context context;

    @BindView(R.id.selected_art_image_view)
    ImageView selectedArtIv;

    public ImageDialog(Context context) {
        super(context);
        this.context = context;

        //makes background transparent
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.image_save_dialog);
        ButterKnife.bind(this);
    }

    public void show(Bitmap cacheBitmap) {
        show();
        selectedArtIv.setImageBitmap(cacheBitmap);
    }
}
