package iacademy.com.safespace.create.connectandcolor;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ConnectAndColorModeActivity extends AppCompatActivity {

    private static final String CONNECT_N_COLOR_MODE = "connect_n_color_mode";
    private static final String CONNECT_N_COLOR_A = "connect_n_color_a";
    private static final String CONNECT_N_COLOR_B = "connect_n_color_b";

    @BindView(R.id.connect_n_color_1_icon_imageview)
    ImageView connectFirstIconIv;

    @BindView(R.id.connect_n_color_2_icon_imageview)
    ImageView connectSecondIconIv;

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_and_color_mode);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.left_imageview)
    public void leftClicked(){
        switchIcon();
    }

    @OnClick(R.id.right_imageview)
    public void rightClicked(){
        switchIcon();
    }

    @OnClick(R.id.connect_n_color_1_icon_imageview)
    public void colorFirstIconIvClicked(){
        Intent intent = new Intent(this, ConnectAndColorActivity.class);
        intent.putExtra(CONNECT_N_COLOR_MODE, CONNECT_N_COLOR_A);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.connect_n_color_2_icon_imageview)
    public void colorSecondIconIvClicked(){
        Intent intent = new Intent(this, ConnectAndColorActivity.class);
        intent.putExtra(CONNECT_N_COLOR_MODE, CONNECT_N_COLOR_B);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    private void switchIcon(){
        if(connectFirstIconIv.getVisibility() == View.VISIBLE){
            setSecondColorIconVisible();
        }else{
            setFirstColorIconVisible();
        }
    }

    private void setFirstColorIconVisible(){
        connectFirstIconIv.setVisibility(View.VISIBLE);
        connectSecondIconIv.setVisibility(View.GONE);
    }

    private void setSecondColorIconVisible(){
        connectSecondIconIv.setVisibility(View.VISIBLE);
        connectFirstIconIv.setVisibility(View.GONE);
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView(this);
        customMenuView.showDialog();
    }
}
