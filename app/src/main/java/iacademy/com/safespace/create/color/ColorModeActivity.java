package iacademy.com.safespace.create.color;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ColorModeActivity extends AppCompatActivity {

    private static final String COLOR_MODE = "color_mode";
    private static final String COLOR_A = "color_a";
    private static final String COLOR_B = "color_b";

    @BindView(R.id.color_1_icon_imageview)
    ImageView colorFirstIconIv;

    @BindView(R.id.color_2_icon_imageview)
    ImageView colorSecondIconIv;

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_mode);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.left_imageview)
    public void leftClicked(){
        switchIcon();
    }

    @OnClick(R.id.right_imageview)
    public void rightClicked(){
        switchIcon();
    }

    @OnClick(R.id.color_1_icon_imageview)
    public void colorFirstIconIvClicked(){
        Intent intent = new Intent(this, ColorActivity.class);
        intent.putExtra(COLOR_MODE, COLOR_A);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.color_2_icon_imageview)
    public void colorSecondIconIvClicked(){
        Intent intent = new Intent(this, ColorActivity.class);
        intent.putExtra(COLOR_MODE, COLOR_B);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    private void switchIcon(){
        if(colorFirstIconIv.getVisibility() == View.VISIBLE){
            setSecondColorIconVisible();
        }else{
            setFirstColorIconVisible();
        }
    }

    private void setFirstColorIconVisible(){
        colorFirstIconIv.setVisibility(View.VISIBLE);
        colorSecondIconIv.setVisibility(View.GONE);
    }

    private void setSecondColorIconVisible(){
        colorSecondIconIv.setVisibility(View.VISIBLE);
        colorFirstIconIv.setVisibility(View.GONE);
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView( this);
        customMenuView.showDialog();
    }
}
