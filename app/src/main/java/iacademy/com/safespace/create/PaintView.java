package iacademy.com.safespace.create;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class PaintView extends View {

    private Path paintPath;
    private Paint drawPaint, canvasPaint;
    private Canvas paintCanvas;
    private int paintColor = 0xFF000000;
    private boolean erase = false;
    private Bitmap canvasBitmap;
    private float brushSize = 10;
    private int width, height;

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
    }

    public int getCurrentWidth() {
        return width;
    }

    public int getCurrentHeight(){
        return height;
    }

    public void setErase(boolean isErase){
        erase = isErase;
        drawPaint.setXfermode( erase ? new PorterDuffXfermode(PorterDuff.Mode.CLEAR) : null);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = w;
        height = h;
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        paintCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(paintPath, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                paintCanvas.drawPath(paintPath, drawPaint);
                paintPath.reset();
                break;
            case MotionEvent.ACTION_DOWN:
                paintPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                paintPath.lineTo(touchX, touchY);
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

    public void setColor(String newColor){
        if(!TextUtils.isEmpty(newColor)){
            invalidate();
            drawPaint.setColor(Color.parseColor(newColor));
        }
    }

    public void setupPaint(){
        paintPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(5);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
        drawPaint.setStrokeWidth(brushSize);
    }

}
