package iacademy.com.safespace.create;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.create.color.ColorModeActivity;
import iacademy.com.safespace.create.connectandcolor.ConnectAndColorModeActivity;
import iacademy.com.safespace.create.gallery.GalleryActivity;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.os.Bundle;

public class CreateActivity extends AppCompatActivity {

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.gallery_button)
    public void galleryClicked(){
        startActivity(new Intent(this, GalleryActivity.class));
    }

    @OnClick(R.id.blank_canvas_button)
    public void blankCanvasClicked(){
        startActivity(new Intent(this, AddBlankCanvasActivity.class));
    }

    @OnClick(R.id.color_button)
    public void colorClicked(){
        startActivity(new Intent(this, ColorModeActivity.class));
    }

    @OnClick(R.id.connect_n_color_button)
    public void connectNColorClicked(){
        startActivity(new Intent(this, ConnectAndColorModeActivity.class));
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView(this);
        customMenuView.showDialog();
    }
}
