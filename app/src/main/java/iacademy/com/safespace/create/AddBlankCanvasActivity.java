package iacademy.com.safespace.create;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AboutActivity;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.settings.FAQSettingsActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.os.Bundle;

public class AddBlankCanvasActivity extends AppCompatActivity {

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_blank_canvas);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.add_image_view)
    public void addClick(){
        startActivity(new Intent(this, BlankCanvasActivity.class));
        finish();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView(this);
        customMenuView.showDialog();
    }
}
