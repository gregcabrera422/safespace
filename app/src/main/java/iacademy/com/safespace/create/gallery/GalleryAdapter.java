package iacademy.com.safespace.create.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.model.ArtworkModel;
import iacademy.com.safespace.utility.FontHelper;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyHolder> {

    private Context context;

    private Callback callback;

    private List<ArtworkModel> artworkModelList;

    public GalleryAdapter(Context context, Callback callback, List<ArtworkModel> artworkModelList) {
        this.context = context;
        this.callback = callback;
        this.artworkModelList = artworkModelList;
    }

    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.art_work_grid_layout, null);
        return new MyHolder(layout, callback, context);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        holder.artIv.setImageBitmap(artworkModelList.get(position).getArtWorkBitmap());
        holder.artTitleTv.setText(artworkModelList.get(position).getFileName());
    }

    @Override
    public int getItemCount() {
        return artworkModelList.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder implements Callback {

        @BindView(R.id.art_image_view)
        ImageView artIv;

        @BindView(R.id.art_title_text_view)
        TextView artTitleTv;

        Callback callback;

        public MyHolder(@NonNull View itemView, Callback callback, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.callback = callback;
            artTitleTv.setTypeface(FontHelper.REGULAR(context));
        }

        @OnClick(R.id.art_image_view)
        public void artClick(){
            artClicked(getAdapterPosition());
        }

        @Override
        public void artClicked(int position) {
            callback.artClicked(position);
        }

    }

    public void filteredList(ArrayList<ArtworkModel> filteredList) {
        artworkModelList = filteredList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void artClicked(int position);
    }

}
