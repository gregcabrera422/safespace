package iacademy.com.safespace.create.connectandcolor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.create.PaintView;
import iacademy.com.safespace.create.gallery.GalleryActivity;
import iacademy.com.safespace.dialog.SavePaintDialog;
import iacademy.com.safespace.utility.PaintFileHelper;
import iacademy.com.safespace.utility.StoragePermissionHelper;
import iacademy.com.safespace.view.CustomMenuView;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class ConnectAndColorActivity extends AppCompatActivity implements SavePaintDialog.Callback {

    private static final int STORAGE_PERMISSION_CODE = 1;
    private static final String CONNECT_N_COLOR_MODE = "connect_n_color_mode";
    private static final String CONNECT_N_COLOR_A = "connect_n_color_a";
    private static final String CONNECT_N_COLOR_B = "connect_n_color_b";

    @BindView(R.id.color_radio_group)
    RadioGroup colorRadioGroup;

    @Nullable
    @BindView(R.id.paintView)
    PaintView paintView;

    @BindView(R.id.paint_background)
    RelativeLayout paintBackgroundView;

    @BindView(R.id.selected_art_image_view)
    ImageView selectedImageView;

    private SavePaintDialog saveDialog;

    private PaintFileHelper paintFileHelper;

    private StoragePermissionHelper storagePermissionHelper;

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_and_color);
        ButterKnife.bind(this);
        geExtras();
        init();
    }

    private void geExtras() {
        if(getIntent().getExtras() != null){
            switch(getIntent().getStringExtra(CONNECT_N_COLOR_MODE)) {
                case CONNECT_N_COLOR_A:
                    selectedImageView.setImageResource(R.drawable.connect_art_1_img_bg);
                    break;
                case CONNECT_N_COLOR_B:
                    selectedImageView.setImageResource(R.drawable.connect_art_2_img_bg);
                    break;
            }
        }
    }

    private void init() {
        paintFileHelper = new PaintFileHelper(this, paintBackgroundView);
        saveDialog = new SavePaintDialog(this, this);
    }

    @OnClick(R.id.color_picker_btn)
    public void colorPickerClicked() {
        if(colorRadioGroup.getVisibility()== View.GONE){
            colorRadioGroup.clearCheck();
            colorRadioGroup.setVisibility(View.VISIBLE);
        }else{
            colorRadioGroup.setVisibility(View.GONE);
        }
    }

    @OnCheckedChanged({R.id.white, R.id.red, R.id.pink, R.id.violet, R.id.blue, R.id.skyBlue, R.id.green, R.id.yellow, R.id.black})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if(checked) {
            String color = button.getTag().toString();
            if(!TextUtils.isEmpty(color)){
                paintView.setColor(color);
            }
            paintView.setErase(false);
            colorRadioGroup.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.erase_btn)
    public void eraseClicked(){
        colorRadioGroup.clearCheck();
        paintView.setErase(true);
    }

    @OnClick(R.id.save_btn)
    public void saveClicked() {
        storagePermissionHelper = new StoragePermissionHelper(this);
        saveDialog.show(paintFileHelper.getCacheBitmap());
    }

    @Override
    public void yesClicked() {
        paintFileHelper.saveColorAndTraceImage(saveDialog.getFileName());
        startActivity(new Intent(this, GalleryActivity.class));
        finish();
    }

    @Override
    public void noClicked() {
        saveDialog.dismiss();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Please allow in permissions to save and view artworks",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView( this);
        customMenuView.showDialog();
    }
}
