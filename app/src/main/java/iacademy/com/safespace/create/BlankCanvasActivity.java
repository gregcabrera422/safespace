package iacademy.com.safespace.create;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.create.gallery.GalleryActivity;
import iacademy.com.safespace.dialog.SavePaintDialog;
import iacademy.com.safespace.utility.PaintFileHelper;
import iacademy.com.safespace.utility.StoragePermissionHelper;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class BlankCanvasActivity extends AppCompatActivity implements SavePaintDialog.Callback {

    private static final int STORAGE_PERMISSION_CODE = 1;

    @BindView(R.id.background)
    RelativeLayout background;

    @BindView(R.id.color_radio_group)
    RadioGroup colorRadioGroup;

    @Nullable
    @BindView(R.id.paintView)
    PaintView paintView;

    private boolean isGrid;

    private SavePaintDialog saveDialog;

    private PaintFileHelper paintFileHelper;

    private StoragePermissionHelper storagePermissionHelper;

    private CustomMenuView customMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank_canvas);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        paintFileHelper = new PaintFileHelper(this, paintView);
        saveDialog = new SavePaintDialog(this, this);
    }

    @OnClick(R.id.color_picker_btn)
    public void colorPickerClicked() {
        if(colorRadioGroup.getVisibility()== View.GONE){
            colorRadioGroup.clearCheck();
            colorRadioGroup.setVisibility(View.VISIBLE);
        }else{
            colorRadioGroup.setVisibility(View.GONE);
        }
    }

    @OnCheckedChanged({R.id.white, R.id.red, R.id.pink, R.id.violet, R.id.blue, R.id.skyBlue, R.id.green, R.id.yellow, R.id.black})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if(checked) {
            String color = button.getTag().toString();
            if(!TextUtils.isEmpty(color)){
                paintView.setColor(color);
            }
            paintView.setErase(false);
            colorRadioGroup.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.erase_btn)
    public void eraseClicked(){
        colorRadioGroup.clearCheck();
        paintView.setErase(true);
    }

    @OnClick(R.id.save_btn)
    public void saveClicked() {
        storagePermissionHelper = new StoragePermissionHelper(this);
        saveDialog.show(paintFileHelper.getCacheBitmap());
    }

    @Override
    public void yesClicked() {
        paintView.setBackgroundColor(getResources().getColor(R.color.canvas_bg_color));
        paintFileHelper.saveImage(saveDialog.getFileName());
        startActivity(new Intent(this, GalleryActivity.class));
        finish();
    }

    @Override
    public void noClicked() {
        saveDialog.dismiss();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.grid_btn)
    void gridClicked(){
        if(isGrid){
            background.setBackgroundResource(R.drawable.blank_canvas_page_bg);
            isGrid = false;
        } else {
            background.setBackgroundResource(R.drawable.create_grid_bg);
            isGrid = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Please allow in permissions to save and view artworks",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView(this);
        customMenuView.showDialog();
    }
}
