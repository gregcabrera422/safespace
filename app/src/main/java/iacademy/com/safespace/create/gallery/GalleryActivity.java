package iacademy.com.safespace.create.gallery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.dialog.ImageDialog;
import iacademy.com.safespace.model.ArtworkModel;
import iacademy.com.safespace.utility.PaintFileHelper;
import iacademy.com.safespace.utility.StoragePermissionHelper;
import iacademy.com.safespace.view.CustomMenuView;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity implements GalleryAdapter.Callback {

    private static final int STORAGE_PERMISSION_CODE = 1;

    @BindView(R.id.search_edit_text)
    EditText searchEt;

    @BindView(R.id.art_works_recycler_view)
    RecyclerView artWorksRv;

    @BindView(R.id.search_button)
    ImageView searchBtn;

    private ArrayList<ArtworkModel> artworkModelList;

    private GridLayoutManager gridLayoutManager;

    private GalleryAdapter galleryAdapter;

    private PaintFileHelper paintFileHelper;

    private StoragePermissionHelper storagePermissionHelper;

    private boolean isSearched;

    private CustomMenuView customMenuView;

    private ImageDialog imageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);
        init();

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    searchClicked();
                    return true;
                }
                return false;
            }
        });

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isSearched = false;
                searchBtn.setBackground(getDrawable(R.drawable.ic_search_white_24dp));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void init() {
        storagePermissionHelper = new StoragePermissionHelper(this);

        artworkModelList = new ArrayList<>();
        paintFileHelper = new PaintFileHelper(this);
        gridLayoutManager = new GridLayoutManager(this, 3);

        artWorksRv.setHasFixedSize(true);
        artWorksRv.setLayoutManager(gridLayoutManager);

        setContents();
    }

    private void setContents() {
        retrieveAllArtWorks();
        galleryAdapter = new GalleryAdapter(this, this, artworkModelList);
        artWorksRv.setAdapter(galleryAdapter);
    }

    private void retrieveAllArtWorks() {
        artworkModelList = paintFileHelper.retrieveAllArtworks();
    }

    @OnClick(R.id.search_button)
    public void searchClicked(){
        if(isSearched){
            searchEt.getText().clear();
            filter(searchEt.getText().toString());
            searchBtn.setBackground(getDrawable(R.drawable.ic_search_white_24dp));
            isSearched = false;
        } else {
            searchBtn.setBackground(getDrawable(R.drawable.ic_close_white_24dp));
            filter(searchEt.getText().toString());
            isSearched = true;
        }
    }

    @OnClick(R.id.add)
    public void addClicked(){
        finish();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    private void filter(String keyword) {
        artworkModelList = paintFileHelper.retrieveAllSearchedArtworks(keyword.toLowerCase());
        System.out.println("size: " + artworkModelList);
        galleryAdapter.filteredList(artworkModelList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setContents();
            } else {
                Toast.makeText(this, "Please allow in permissions to save and view artworks", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView(this);
        customMenuView.showDialog();
    }

    @Override
    public void artClicked(int position) {
        imageDialog = new ImageDialog(this);
        imageDialog.show(artworkModelList.get(position).getArtWorkBitmap());
    }
}
