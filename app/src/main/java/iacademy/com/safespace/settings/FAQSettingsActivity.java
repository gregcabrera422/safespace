package iacademy.com.safespace.settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.know.KnowBlankActivity;
import iacademy.com.safespace.know.MentalOrgAdapter;
import iacademy.com.safespace.utility.FontHelper;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.List;

public class FAQSettingsActivity extends AppCompatActivity {

    @BindView(R.id.faq_settings_recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqsettings);
        ButterKnife.bind(this);

        ((TextView)findViewById(R.id.faqSettingTextView)).setTypeface(FontHelper.BOLD(FAQSettingsActivity.this));

        List<String> questions = Arrays.asList(getResources().getStringArray(R.array.faq_settings_questions));
        List<String> answers = Arrays.asList(getResources().getStringArray(R.array.faq_settings_answers));

        recyclerView.setLayoutManager(new LinearLayoutManager(FAQSettingsActivity.this));
        FAQSettingsAdapter adapter = new FAQSettingsAdapter(FAQSettingsActivity.this, questions, answers);
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }
}
