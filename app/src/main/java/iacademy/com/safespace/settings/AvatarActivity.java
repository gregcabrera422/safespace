package iacademy.com.safespace.settings;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import iacademy.com.safespace.R;
import iacademy.com.safespace.model.AvatarModel;
import iacademy.com.safespace.model.AvatarPartsModel;
import iacademy.com.safespace.utility.Assets;
import iacademy.com.safespace.utility.SharedPreferencesHelper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;

public class AvatarActivity extends AppCompatActivity {
    @BindView(R.id.avatar_imageview)
    ImageView avatar_imageview;
    AvatarModel avatar;
    FragmentManager fm;
    Fragment f;
    int pointer;
    String stringPointer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);
        ButterKnife.bind(this);
        fm = getSupportFragmentManager();
        Gson gson = new Gson();
        String json = new SharedPreferencesHelper(this).getSharedPrefs.getString("avatar", null);
        avatar = new AvatarModel(this);
        if (json == null){
            avatar = new AvatarModel(this);
            avatar.setEyes(Assets.getEyes().get(0));
            avatar.setHair(Assets.getHair().get(0));
            avatar.setLips(Assets.getLips().get(0));
            avatar.setNose(Assets.getNose().get(0));
            avatar.setShirt(Assets.getShirts().get(0));
            avatar.setSkin(Assets.getSkins().get(0));

            avatar_imageview.setImageBitmap(avatar.generateAvatar());
        }else{
            Type type = new TypeToken<List<AvatarPartsModel>>(){}.getType();
            avatar.parseArrayList(gson.fromJson(json, type));
            avatar_imageview.setImageBitmap(avatar.generateAvatar());
        }
        avatar_imageview.setImageBitmap(avatar.generateAvatar());
        f = AvatarPartsFragment.newInstance("eyes", 1);
        pointer = 3;
        stringPointer = "eyes";
        fm.beginTransaction().replace(R.id.fragment_container, f).commit();
    }

    public void changeTab(View view){
        switch (view.getId()){
            case R.id.skin:
                f = AvatarPartsFragment.newInstance("skin", 1);
                pointer = 0;
                stringPointer = "skin";
                break;
            case R.id.shirt:
                f = AvatarPartsFragment.newInstance("shirt", 1);
                pointer = 1;
                stringPointer = "shirt";
                break;
            default:
                f = AvatarPartsFragment.newInstance("hair", 1);
                pointer = 2;
                stringPointer = "hair";
                break;
            case R.id.eyes:
                f = AvatarPartsFragment.newInstance("eyes", 1);
                pointer = 3;
                stringPointer = "eyes";
                break;
            case R.id.nose:
                f = AvatarPartsFragment.newInstance("nose", 1);
                pointer = 4;
                stringPointer = "nose";
                break;
            case R.id.mouth:
                f = AvatarPartsFragment.newInstance("lips", 1);
                pointer = 5;
                stringPointer = "lips";
                break;
        }
        fm.beginTransaction().replace(R.id.fragment_container, f).commit();
        Log.v("fragments", fm.getFragments().toString());
    }
    public void changeAsset(View view){
        switch (view.getId()){
            case R.id.black_btn:
                f = AvatarPartsFragment.newInstance(stringPointer, 1);
                break;
            case R.id.brown_btn:
                f = AvatarPartsFragment.newInstance(stringPointer, 2);
                break;
        }
        fm.beginTransaction().replace(R.id.fragment_container, f).commit();
    }

    public void updateAvatar(View view){
        avatar.updateAvatar(pointer, view.getId(), view.getTag().toString());
        avatar_imageview.setImageBitmap(avatar.generateAvatar());
    }

    @Override
    public void onBackPressed() {
        Gson gson = new Gson();
        new SharedPreferencesHelper(this).Write("avatar", gson.toJson(avatar.getSavable()));
        finish();
    }
}
