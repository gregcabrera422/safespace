package iacademy.com.safespace.settings;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.Assets;
import iacademy.com.safespace.model.AvatarPartsModel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import static iacademy.com.safespace.utility.Assets.filterSet1;
import static iacademy.com.safespace.utility.Assets.filterSet2;

public class AvatarPartsFragment extends Fragment {
    ArrayList<ImageView> imageViews = new ArrayList<ImageView>();
    ArrayList<LinearLayout>linearLayouts = new ArrayList<LinearLayout>();
    LinearLayout layout1;
    ArrayList<AvatarPartsModel> parts;
    LinearLayout layout;
    String part;
    View view;
    int set;

    public AvatarPartsFragment() {
        // Required empty public constructor
    }

    public static AvatarPartsFragment newInstance(String part, int set){
        AvatarPartsFragment f = new AvatarPartsFragment();
        Bundle args = new Bundle();
        args.putString("pointer", part);
        switch (part){
            case "eyes":
                args.putParcelableArrayList("parts", Assets.getEyes());
                break;
            case "lips":
                args.putParcelableArrayList("parts", Assets.getLips());
                break;
            case "nose":
                args.putParcelableArrayList("parts", Assets.getNose());
                break;
            case "skin":
                args.putParcelableArrayList("parts", Assets.getSkins());
                break;
            case "shirt":
                args.putParcelableArrayList("parts", Assets.getShirts());
                break;
            case "hair":
                args.putParcelableArrayList("parts", Assets.getHair());
                break;
        }
        args.putString("part", part);
        args.putInt("set", set);
        f.setArguments(args);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_avatar_parts, container, false);
        imageViews.add(view.findViewById(R.id.part1));
        imageViews.add(view.findViewById(R.id.part2));
        imageViews.add(view.findViewById(R.id.part3));
        imageViews.add(view.findViewById(R.id.part4));
        imageViews.add(view.findViewById(R.id.part5));
        imageViews.add(view.findViewById(R.id.part6));
        linearLayouts.add(view.findViewById(R.id.layout1));
        linearLayouts.add(view.findViewById(R.id.layout2));
        linearLayouts.add(view.findViewById(R.id.layout3));
        linearLayouts.add(view.findViewById(R.id.layout4));
        linearLayouts.add(view.findViewById(R.id.layout5));
        linearLayouts.add(view.findViewById(R.id.layout6));
        parts = getArguments().getParcelableArrayList("parts");
        part = getArguments().getString("part");
        set = getArguments().getInt("set");

        layout = (LinearLayout)view.findViewById(R.id.btn_layout);
        if (!part.equals("eyes") && !part.equals("hair")){
            layout.setVisibility(View.GONE);
        }

        if (set == 1){
            setImages(filterSet1(parts), "black");
        }else{
            setImages(filterSet2(parts), "brown");
        }

        return view;
    }

    public void setImages(ArrayList<AvatarPartsModel> parts, String tag){
        for (int i = 0; i < imageViews.size(); ++i){
            if (part == "shirt"){
                ArrayList<AvatarPartsModel> clothes = Assets.getDisplayClothes();
                linearLayouts.get(i).setTag(tag);
                imageViews.get(i).setImageResource(clothes.get(i).getImage());
            }else if (part == "skin"){
                ArrayList<AvatarPartsModel> skin = Assets.getDisplaySkin();
                linearLayouts.get(i).setTag(tag);
                imageViews.get(i).setImageResource(skin.get(i).getImage());
            }else{
                linearLayouts.get(i).setTag(tag);
                imageViews.get(i).setImageResource(parts.get(i).getImage());
            }
        }
    }

}
