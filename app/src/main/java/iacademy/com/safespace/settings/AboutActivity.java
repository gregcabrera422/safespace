package iacademy.com.safespace.settings;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {
    @BindView(R.id.aboutSettingTextView)
    TextView aboutSettingTextView;

    @BindView(R.id.appDescriptionTextView)
    TextView appDescriptionTextView;

    @BindView(R.id.designedByTextView)
    TextView designedByTextView;

    @BindView(R.id.designers)
    TextView designers;

    @BindView(R.id.developmentByTextView)
    TextView developmentByTextView;

    @BindView(R.id.developers)
    TextView developers;

    @BindView(R.id.activitiesTextView)
    TextView activitiesTextView;

    @BindView(R.id.activities)
    TextView activities;

    @BindView(R.id.musicTextView)
    TextView musicTextView;

    @BindView(R.id.music)
    TextView music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        aboutSettingTextView.setTypeface(FontHelper.BOLD(this));
        appDescriptionTextView.setTypeface(FontHelper.REGULAR(this));
        designedByTextView.setTypeface(FontHelper.BOLD(this));
        designers.setTypeface(FontHelper.BOLD(this));
        developmentByTextView.setTypeface(FontHelper.BOLD(this));
        developers.setTypeface(FontHelper.BOLD(this));
        activitiesTextView.setTypeface(FontHelper.BOLD(this));
        activities.setTypeface(FontHelper.ITALIC(this));
        musicTextView.setTypeface(FontHelper.BOLD(this));
        music.setTypeface(FontHelper.ITALIC(this));

    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }
}
