package iacademy.com.safespace.settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class FAQSettingsAdapter extends RecyclerView.Adapter<FAQSettingsAdapter.ViewHolder> {

    private List<String> question;
    private List<String> answer;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    FAQSettingsAdapter(Context context, List<String> question, List<String> answer) {
        this.mInflater = LayoutInflater.from(context);
        this.question = question;
        this.answer = answer;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.faq_settings_item_view, parent, false);
        ((TextView)view.findViewById(R.id.faq_settings_question)).setTypeface(FontHelper.BOLD(context));
        ((TextView)view.findViewById(R.id.faq_settings_answer)).setTypeface(FontHelper.REGULAR(context));
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String question = this.question.get(position);
        String answer = this.answer.get(position);
        holder.question.setText(question);
        holder.answer.setText(answer);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return question.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView question;
        TextView answer;

        ViewHolder(View itemView) {
            super(itemView);
            question = itemView.findViewById(R.id.faq_settings_question);
            answer = itemView.findViewById(R.id.faq_settings_answer);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return question.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}