package iacademy.com.safespace.meditate.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.meditate.breathing.VideoPlayerActivity;
import iacademy.com.safespace.moodcheckin.CheckInHomeActivity;
import iacademy.com.safespace.moodcheckin.FeelingBetterActivity;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.scwang.wave.MultiWaveHeader;

public class MusicPlayerActivity extends AppCompatActivity {

    private static final String STRING_MESSAGE = "string_message";
    private static final String PMR_MESSAGE = "pmr";
    private static final String BODY_SCAN_MESSAGE = "bodyScan";
    private static final String SAFE_PLACE_MESSAGE = "safePlace";
    private static final int FORWARD_BACKWARD_VALUE = 5000;

    @BindView(R.id.musicLogo_imageview)
    ImageView musicLogoImageView;

    @BindView(R.id.musicTitle_textview)
    TextView musicTitleTextView;

    @BindView(R.id.music_length_textview)
    TextView musicLengthTextView;

    @BindView(R.id.seekbar)
    SeekBar seekbar;

    @BindView(R.id.playPause_imageview)
    ImageView playPauseImageView;

    @BindView(R.id.waveA)
    MultiWaveHeader waveA;

    @BindView(R.id.waveB)
    MultiWaveHeader waveB;

    private MediaPlayer mediaPlayer;

    private Runnable runnable;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        ButterKnife.bind(this);

        musicTitleTextView.setTypeface(FontHelper.BOLD(this));
        musicLengthTextView.setTypeface(FontHelper.REGULAR(this));

        getExtras();
        addListeners();
        waveA.start();
        waveB.start();

    }

    private void getExtras() {
        if(getIntent().getExtras() != null){
            handler = new Handler();

            switch(getIntent().getStringExtra(STRING_MESSAGE)){
                case PMR_MESSAGE:
                    musicTitleTextView.setText(getResources().getString(R.string.pmr_label));
                    musicLengthTextView.setText(getResources().getString(R.string.pmr_length));
                    musicLogoImageView.setImageResource(R.drawable.pmr_logo);
                    mediaPlayer = MediaPlayer.create(this, R.raw.pmr);
                    break;
                case BODY_SCAN_MESSAGE:
                    musicTitleTextView.setText(getResources().getString(R.string.body_scan_label));
                    musicLengthTextView.setText(getResources().getString(R.string.body_scan_length));
                    musicLogoImageView.setImageResource(R.drawable.body_scan_logo);
                    mediaPlayer = MediaPlayer.create(this, R.raw.body_scan);
                    break;
                case SAFE_PLACE_MESSAGE:
                    musicTitleTextView.setText(getResources().getString(R.string.safe_place_label));
                    musicLengthTextView.setText(getResources().getString(R.string.safe_place_length));
                    musicLogoImageView.setImageResource(R.drawable.safe_place_logo);
                    mediaPlayer = MediaPlayer.create(this, R.raw.imagery);
                    break;
            }
        }
    }

    private void addListeners() {
        mediaPlayer.setOnPreparedListener(mediaPlayer1 -> {
                seekbar.setMax(mediaPlayer1.getDuration());
                mediaPlayer1.start();
                changeSeekbar();
        });


        if(seekbar != null){
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b){
                        mediaPlayer.seekTo(i);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    private void changeSeekbar() {
        if(mediaPlayer != null){
            seekbar.setProgress(mediaPlayer.getCurrentPosition());
        }


        if(mediaPlayer != null && mediaPlayer.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    changeSeekbar();
                }
            };

            handler.postDelayed(runnable, 1000);
        }
    }

    @OnClick(R.id.playPause_imageview)
    public void playPauseClicked(){
        if(mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                waveA.stop();
                waveB.stop();
                playPauseImageView.setImageResource(R.drawable.play_btn);
            } else {
                mediaPlayer.start();
                waveA.start();
                waveB.start();
                playPauseImageView.setImageResource(R.drawable.pause_btn);
                changeSeekbar();
            }
        }
    }

    @OnClick(R.id.forward_imageview)
    public void forwardClicked(){
        if(mediaPlayer != null) {
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + FORWARD_BACKWARD_VALUE);
        }
    }

    @OnClick(R.id.back_imageview)
    public void backClicked(){
        if(mediaPlayer != null){
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - FORWARD_BACKWARD_VALUE);
        }
    }

    @Override
    public void onBackPressed() {
        if(mediaPlayer != null){
            mediaPlayer.stop();
        }

        boolean isSkipClicked = new SharedPreferencesHelper(MusicPlayerActivity.this).getSharedPrefs.getBoolean("isSkipClicked", false);
        if (!isSkipClicked){
            new SharedPreferencesHelper(MusicPlayerActivity.this).Write("isSkipClicked", true);
            Intent intent = new Intent(MusicPlayerActivity.this, FeelingBetterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mediaPlayer != null){
            mediaPlayer.pause();
            waveA.stop();
            waveB.stop();
        }
        playPauseImageView.setImageResource(R.drawable.play_btn);
    }

    @OnClick(R.id.back_btn)
    public void backActivityClicked(){
        onBackPressed();
        finish();
    }
}
