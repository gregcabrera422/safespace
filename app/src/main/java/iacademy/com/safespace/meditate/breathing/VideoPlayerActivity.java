package iacademy.com.safespace.meditate.breathing;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.meditate.musicplayer.MusicPlayerActivity;
import iacademy.com.safespace.moodcheckin.CheckInHomeActivity;
import iacademy.com.safespace.moodcheckin.FeelingBetterActivity;
import iacademy.com.safespace.track.MoodActivity;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.VideoView;

public class VideoPlayerActivity extends AppCompatActivity {

    private static final String BREATHING_MODE = "breathing_mode";
    private static final String BREATHING_A = "breathing_a";
    private static final String BREATHING_B = "breathing_b";
    private static final String videoUrlA =  "android.resource://iacademy.com.safespace/" + R.raw.breathing_a;
    private static final String videoUrlB =  "android.resource://iacademy.com.safespace/" + R.raw.breathing_b;
    private static final int FORWARD_BACKWARD_VALUE = 10000;

    @BindView(R.id.seekbar)
    SeekBar seekbar;

    @BindView(R.id.playPause_imageview)
    ImageView playPauseImageView;

    @BindView(R.id.musicVideo_videoview)
    VideoView musicVideo;

    private Runnable runnable;

    private Handler handler;

    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);
        getExtras();
        addListeners();
    }

    private void getExtras() {
        if (getIntent().getExtras() != null) {
            handler = new Handler();
            getWindow().setFormat(PixelFormat.UNKNOWN);

            switch (getIntent().getStringExtra(BREATHING_MODE)) {
                case BREATHING_A:
                    uri = Uri.parse(videoUrlA);
                    break;
                case BREATHING_B:
                    uri = Uri.parse(videoUrlB);
                    break;
            }

            musicVideo.setVideoURI(uri);
            musicVideo.requestFocus();
            musicVideo.start();
        }
    }

    private void addListeners() {
        musicVideo.setOnPreparedListener(musicVideo1 -> {
            seekbar.setMax(musicVideo1.getDuration());
            musicVideo1.start();
            changeSeekbar();
        });

        if(seekbar != null){
            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b){
                        musicVideo.seekTo(i);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    private void changeSeekbar() {
        if(musicVideo != null){
            seekbar.setProgress(musicVideo.getCurrentPosition());
        }


        if(musicVideo != null && musicVideo.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    changeSeekbar();
                }
            };

            handler.postDelayed(runnable, 1000);
        }
    }

    @OnClick(R.id.playPause_imageview)
    public void playPauseClicked(){
        if(musicVideo != null){
            if(musicVideo.isPlaying()){
                musicVideo.pause();
                playPauseImageView.setImageResource(R.drawable.play_btn);
            } else {
                musicVideo.start();
                playPauseImageView.setImageResource(R.drawable.pause_btn);
                changeSeekbar();
            }
        }
    }

    @OnClick(R.id.forward_imageview)
    public void forwardClicked(){
        if(musicVideo != null){
            musicVideo.seekTo(musicVideo.getCurrentPosition() + FORWARD_BACKWARD_VALUE);
        }
    }

    @OnClick(R.id.back_imageview)
    public void backClicked(){
        if(musicVideo != null){
            musicVideo.seekTo(musicVideo.getCurrentPosition() - FORWARD_BACKWARD_VALUE);
        }
    }

    @Override
    public void onBackPressed() {
        if(musicVideo != null){
            musicVideo.stopPlayback();
        }

        boolean isSkipClicked = new SharedPreferencesHelper(VideoPlayerActivity.this).getSharedPrefs.getBoolean("isSkipClicked", false);
        if (!isSkipClicked){
            new SharedPreferencesHelper(VideoPlayerActivity.this).Write("isSkipClicked", true);
            Intent intent = new Intent(VideoPlayerActivity.this, FeelingBetterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(musicVideo != null){
            musicVideo.pause();
        }
    }

    @OnClick(R.id.back_btn)
    public void backActivityClicked(){
        onBackPressed();
        finish();
    }
}
