package iacademy.com.safespace.meditate;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.meditate.breathing.BreathingActivity;
import iacademy.com.safespace.meditate.musicplayer.MusicPlayerActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MeditateActivity extends AppCompatActivity {

    private static final String STRING_MESSAGE = "string_message";
    private static final String BREATHING_MESSAGE = "breathing";
    private static final String PMR_MESSAGE = "pmr";
    private static final String BODY_SCAN_MESSAGE = "bodyScan";
    private static final String SAFE_PLACE_MESSAGE = "safePlace";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meditate);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.breathing_button, R.id.pmr_button, R.id.bodyScan_button, R.id.safePlace_button})
    public void onItemClicked(View view){
        Intent musicPlayerIntent = new Intent(this, MusicPlayerActivity.class);
        Intent breathingIntent = new Intent(this, BreathingActivity.class);

        switch(view.getId()){
            case R.id.breathing_button:
                breathingIntent.putExtra(STRING_MESSAGE, BREATHING_MESSAGE);
                startActivity(breathingIntent);
                break;
            case R.id.pmr_button:
                musicPlayerIntent.putExtra(STRING_MESSAGE, PMR_MESSAGE);
                startActivity(musicPlayerIntent);
                break;
            case R.id.bodyScan_button:
                musicPlayerIntent.putExtra(STRING_MESSAGE, BODY_SCAN_MESSAGE);
                startActivity(musicPlayerIntent);
                break;
            case R.id.safePlace_button:
                musicPlayerIntent.putExtra(STRING_MESSAGE, SAFE_PLACE_MESSAGE);
                startActivity(musicPlayerIntent);
                break;
        }
    }

    @OnClick(R.id.back_btn)
    public void backActivityClicked(){
        onBackPressed();
        finish();
    }
}
