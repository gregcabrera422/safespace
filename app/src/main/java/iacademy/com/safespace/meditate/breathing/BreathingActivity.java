package iacademy.com.safespace.meditate.breathing;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class BreathingActivity extends AppCompatActivity {

    private static final String BREATHING_MODE = "breathing_mode";
    private static final String BREATHING_A = "breathing_a";
    private static final String BREATHING_B = "breathing_b";

    @BindView(R.id.breathing_icon_imageview)
    ImageView breathingFirstIconIv;

    @BindView(R.id.breathing_2_icon_imageview)
    ImageView breathingSecondIconIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathing);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.left_imageview)
    public void leftClicked(){
        switchIcon();
    }

    @OnClick(R.id.right_imageview)
    public void rightClicked(){
        switchIcon();
    }

    @OnClick(R.id.breathing_icon_imageview)
    public void breathingFirstIconIv(){
        Intent intent = new Intent(this, VideoPlayerActivity.class);
        intent.putExtra(BREATHING_MODE, BREATHING_A);
        startActivity(intent);
    }

    @OnClick(R.id.breathing_2_icon_imageview)
    public void breathingSecondIconIv(){
        Intent intent = new Intent(this, VideoPlayerActivity.class);
        intent.putExtra(BREATHING_MODE, BREATHING_B);
        startActivity(intent);
    }

    private void switchIcon(){
        if(breathingFirstIconIv.getVisibility() == View.VISIBLE){
            setSecondBreathingIconVisible();
        }else{
            setFirstBreathingIconVisible();
        }
    }

    private void setFirstBreathingIconVisible(){
        breathingFirstIconIv.setVisibility(View.VISIBLE);
        breathingSecondIconIv.setVisibility(View.GONE);
    }

    private void setSecondBreathingIconVisible(){
        breathingSecondIconIv.setVisibility(View.VISIBLE);
        breathingFirstIconIv.setVisibility(View.GONE);
    }

    @OnClick(R.id.back_btn)
    public void backActivityClicked(){
        onBackPressed();
        finish();
    }

}
