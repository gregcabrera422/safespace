package iacademy.com.safespace.know;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.List;

public class ConsultationCentersActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private ConsultationCentersViewPagerAdapter consultationCentersViewPagerAdapter;
    private View headerview;
    private List<String> consultationCenters;

    @BindView(R.id.know_nav_view)
    NavigationView navView;

    @BindView(R.id.consultation_centers_viewpager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation_centers);

        ButterKnife.bind(this);

        ((TextView)findViewById(R.id.consultation_centers_textView)).setTypeface(FontHelper.BOLD(this));

        consultationCenters = Arrays.asList(getResources().getStringArray(R.array.consultation_centers));
        consultationCentersViewPagerAdapter = new ConsultationCentersViewPagerAdapter(this, consultationCenters);
        viewPager.setAdapter(consultationCentersViewPagerAdapter);

        drawerLayout = findViewById(R.id.drawer_layout);

        //handle onClick of nav Drawer
        headerview = navView.getHeaderView(0);

        headerview.findViewById(R.id.nav_mental_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(ConsultationCentersActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "mental");
                startActivity(intent);
            }
        });

        headerview.findViewById(R.id.nav_consultation_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        headerview.findViewById(R.id.nav_crisis_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(ConsultationCentersActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "crisis");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.RIGHT)){
            drawerLayout.closeDrawers();
        } else{
            super.onBackPressed();
        }
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    void onHamburgerClicked(){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    @OnClick(R.id.consultation_previous_place_btn)
    void onPreviousPlaceClicked(){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

    @OnClick(R.id.consultation_next_place_btn)
    void onNextPlaceClicked(){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    @OnClick(R.id.consultation_next_btn)
    void onNextBtnClicked(){
        Intent intent = new Intent(ConsultationCentersActivity.this, KnowBlankActivity.class);
        intent.putExtra("listType", "center");
        intent.putExtra("centerPlace", viewPager.getCurrentItem());
        startActivity(intent);
    }

    public void setHamburgerNavFont(){
        ((TextView)headerview.findViewById(R.id.nav_factsheet_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_mental_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_consultation_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_crisis_textView)).setTypeface(FontHelper.BOLD(this));
    }
}
