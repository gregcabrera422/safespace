package iacademy.com.safespace.know;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.List;

public class KnowBlankActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private RecyclerView recyclerView;
    private LinearLayout header;
    private View headerview;
    private ImageView blank_header_imageview;
    private TextView blank_header_textview;
    private static int y;

    @BindView(R.id.know_nav_view)
    NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know_blank);

        ButterKnife.bind(this);

        header = findViewById(R.id.blank_header);
        drawerLayout = findViewById(R.id.drawer_layout);

        blank_header_imageview = findViewById(R.id.blank_header_imageview);
        blank_header_textview = findViewById(R.id.blank_header_textview);
        blank_header_textview.setTypeface(FontHelper.BOLD(KnowBlankActivity.this));

        String listType = getIntent().getExtras().getString("listType");

        switch (listType){
            case "mental":
                header.setVisibility(View.VISIBLE);

                blank_header_imageview.setImageResource(R.drawable.mhsg_logo);
                blank_header_textview.setText(R.string.drawer_item_mental);
                blank_header_textview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                List<String> mhsg = Arrays.asList(getResources().getStringArray(R.array.mentalHealthSupportGroups));

                recyclerView = findViewById(R.id.blank_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(KnowBlankActivity.this));
                MentalOrgAdapter mentalOrgAdapter = new MentalOrgAdapter(KnowBlankActivity.this, mhsg);
                /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);

                        if (!recyclerView.canScrollVertically(1)) {
                            Toast.makeText(KnowBlankActivity.this, "Last", Toast.LENGTH_SHORT).show();

                        }

                        if(recyclerView.SCROLL_STATE_IDLE==newState){
                            // fragProductLl.setVisibility(View.VISIBLE);
                            if(y<=0){
                                //fragProductLl.setVisibility(View.VISIBLE);
                                Toast.makeText(KnowBlankActivity.this, "Scrolled up", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                y=0;
                                //fragProductLl.setVisibility(View.GONE);
                            }
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                        super.onScrolled(recyclerView, dx, dy);
                        y=dy;
                    }
                });*/

                recyclerView.setAdapter(mentalOrgAdapter);
                break;
            case "center":
                header.setVisibility(View.GONE);

                List<String> centerName = null;
                List<String> centerDescription = null;

                int place = getIntent().getExtras().getInt("centerPlace", -1);

                switch (place){
                    case 0:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_batangas));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_batangas));
                        break;
                    case 1:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_makati));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_makati));
                        break;
                    case 2:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_mandaluyong));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_mandaluyong));
                        break;
                    case 3:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_manila));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_manila));
                        break;
                    case 4:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_marikina));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_marikina));
                        break;
                    case 5:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_pasig));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_pasig));
                        break;
                    case 6:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName_quezon));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription_quezon));
                        break;
                    default:
                        centerName = Arrays.asList(getResources().getStringArray(R.array.centersName));
                        centerDescription = Arrays.asList(getResources().getStringArray(R.array.centersDescription));
                        break;
                }

                recyclerView = findViewById(R.id.blank_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(KnowBlankActivity.this));
                ConsultationCenterAdapter consultationCenterAdapter = new ConsultationCenterAdapter(KnowBlankActivity.this, centerName, centerDescription);
                //adapter.setClickListener(KnowActivity.this);
                recyclerView.setAdapter(consultationCenterAdapter);
                break;
            case "crisis":
                header.setVisibility(View.VISIBLE);

                blank_header_imageview.setImageResource(R.drawable.hotline_logo);
                blank_header_textview.setText(R.string.drawer_item_crisis);
                blank_header_textview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                List<String> crisisName = Arrays.asList(getResources().getStringArray(R.array.crisisHotlinesName));
                List<String> crisisDescription = Arrays.asList(getResources().getStringArray(R.array.crisisHotlinesDescription));

                recyclerView = findViewById(R.id.blank_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(KnowBlankActivity.this));
                CrisisHotlinesAdapter crisisHotlinesAdapter = new CrisisHotlinesAdapter(KnowBlankActivity.this, crisisName, crisisDescription);
                //adapter.setClickListener(KnowActivity.this);
                recyclerView.setAdapter(crisisHotlinesAdapter);
                break;
        }

        //handle onClick of nav Drawer
        headerview = navView.getHeaderView(0);
        setHamburgerNavFont();

        headerview.findViewById(R.id.nav_mental_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                header.setVisibility(View.VISIBLE);

                blank_header_imageview.setImageResource(R.drawable.mhsg_logo);
                blank_header_textview.setText(R.string.drawer_item_mental);
                blank_header_textview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                List<String> questions = Arrays.asList(getResources().getStringArray(R.array.mentalHealthSupportGroups));

                RecyclerView recyclerView = findViewById(R.id.blank_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(KnowBlankActivity.this));
                MentalOrgAdapter adapter = new MentalOrgAdapter(KnowBlankActivity.this, questions);
                //adapter.setClickListener(KnowActivity.this);
                recyclerView.setAdapter(adapter);
            }
        });

        headerview.findViewById(R.id.nav_consultation_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                startActivity(new Intent(KnowBlankActivity.this, ConsultationCentersActivity.class));
            }
        });

        headerview.findViewById(R.id.nav_crisis_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                header.setVisibility(View.VISIBLE);

                blank_header_imageview.setImageResource(R.drawable.hotline_logo);
                blank_header_textview.setText(R.string.drawer_item_crisis);
                blank_header_textview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

                List<String> crisisName = Arrays.asList(getResources().getStringArray(R.array.crisisHotlinesName));
                List<String> crisisDescription = Arrays.asList(getResources().getStringArray(R.array.crisisHotlinesDescription));

                RecyclerView recyclerView = findViewById(R.id.blank_recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(KnowBlankActivity.this));
                CrisisHotlinesAdapter adapter = new CrisisHotlinesAdapter(KnowBlankActivity.this, crisisName, crisisDescription);
                //adapter.setClickListener(KnowActivity.this);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.RIGHT)){
            drawerLayout.closeDrawers();
        } else{
            super.onBackPressed();
        }
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
    }

    @OnClick(R.id.hamburger_btn)
    void onHamburgerClicked(){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    public void setHamburgerNavFont(){
        ((TextView)headerview.findViewById(R.id.nav_factsheet_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_mental_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_consultation_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_crisis_textView)).setTypeface(FontHelper.BOLD(this));
    }
}
