package iacademy.com.safespace.know;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import iacademy.com.safespace.R;
import iacademy.com.safespace.moodcheckin.CheckInActivity1;
import iacademy.com.safespace.moodcheckin.CheckInActivity2;
import iacademy.com.safespace.moodcheckin.CheckInHomeActivity;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KnowActivity extends AppCompatActivity implements FAQAdapter.ItemClickListener {
    private PopupWindow mPopupWindow = null;
    private DrawerLayout drawerLayout;
    private View headerview;

    @BindView(R.id.know_nav_view)
    NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know);

        //set Font
        ((TextView)findViewById(R.id.terms_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.faq_textView)).setTypeface(FontHelper.BOLD(this));

        ButterKnife.bind(this);

        drawerLayout = findViewById(R.id.drawer_layout);

        //FAQs Section
        List<String> questions = Arrays.asList(getResources().getStringArray(R.array.know_faq_questions));

        RecyclerView recyclerView = findViewById(R.id.know_faq_recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);
        FAQAdapter adapter = new FAQAdapter(this, questions);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        headerview = navView.getHeaderView(0);
        setHamburgerNavFont();

        headerview.findViewById(R.id.nav_mental_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(KnowActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "mental");
                startActivity(intent);
            }
        });

        headerview.findViewById(R.id.nav_consultation_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                startActivity(new Intent(KnowActivity.this, ConsultationCentersActivity.class));
            }
        });

        headerview.findViewById(R.id.nav_crisis_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(KnowActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "crisis");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(KnowActivity.this, FAQDetailsActivity.class);
        intent.putExtra("faqID", position);
        startActivity(intent);
    }

    private void showPopUp(String word, String meaning) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.know_popup_terms, null);
        TextView title = customView.findViewById(R.id.know_popup_title);
        TextView description = customView.findViewById(R.id.know_popup_description);

        title.setTypeface(FontHelper.BOLD(KnowActivity.this));
        description.setTypeface(FontHelper.REGULAR(KnowActivity.this));

        title.setText(word);
        description.setText(meaning);

        // Initialize a new instance of popup window
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        LinearLayout pop_up_layout = (LinearLayout) customView.findViewById(R.id.know_pop_up_layout);
        FrameLayout know_pop_up_window = (FrameLayout) customView.findViewById(R.id.know_pop_up_window);

        // Set a click listener for the popup window close button
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(know_pop_up_window)) {
                    // do something
                } else {
                    onBackPressed();
                }
            }
        };

        pop_up_layout.setOnClickListener(listener);
        know_pop_up_window.setOnClickListener(listener);

        // Finally, show the popup window at the center location of root relative layout
        mPopupWindow.showAtLocation(findViewById(R.id.know_root_layout), Gravity.CENTER, 0, 0);
    }

    @Override
    public void onBackPressed() {
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        } else if(drawerLayout.isDrawerOpen(Gravity.RIGHT)){
            drawerLayout.closeDrawers();
        } else{
            super.onBackPressed();
        }
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    void onHamburgerClicked(){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    @OnClick(R.id.know_terms_anxiety)
    void onAnxietyClicked(){
        showPopUp("Anxiety Disorder", getString(R.string.Anxiety_Disorder));
    }

    @OnClick(R.id.know_terms_stress)
    void onStressClicked(){
        showPopUp("Stress", getString(R.string.stress));
    }

    @OnClick(R.id.know_terms_worry)
    void onWorryClicked(){
        showPopUp("Worry", getString(R.string.worry));
    }

    @OnClick(R.id.know_terms_mental)
    void onMentalClicked(){
        showPopUp("Mental Health Disorder", getString(R.string.Mental_Health_Disorder));
    }
    @OnClick(R.id.know_terms_depression)
    void onDepressionClicked(){
        showPopUp("Depression", getString(R.string.depression));
    }
    @OnClick(R.id.know_terms_cognitive)
    void onCognitiveClicked(){
        showPopUp("Cognitive Distortions", getString(R.string.Cognitive_Distortions));
    }

    public void setHamburgerNavFont(){
        ((TextView)headerview.findViewById(R.id.nav_factsheet_textView)).setTypeface(FontHelper.BOLD(KnowActivity.this));
        ((TextView)headerview.findViewById(R.id.nav_mental_textView)).setTypeface(FontHelper.BOLD(KnowActivity.this));
        ((TextView)headerview.findViewById(R.id.nav_consultation_textView)).setTypeface(FontHelper.BOLD(KnowActivity.this));
        ((TextView)headerview.findViewById(R.id.nav_crisis_textView)).setTypeface(FontHelper.BOLD(KnowActivity.this));
    }

}
