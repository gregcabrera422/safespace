package iacademy.com.safespace.know;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;

public class ConsultationCentersViewPagerAdapter extends PagerAdapter {

    @BindView(R.id.consultation_place_textview)
    TextView place;

    private LayoutInflater layoutInflater;

    private Context context;

    private List<String> consultationModelList;

    public ConsultationCentersViewPagerAdapter(Context context, List<String> data) {
        this.context = context;
        consultationModelList = data;


    }

    @Override
    public int getCount() {
        return consultationModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.consultation_item_view, container, false);
        ButterKnife.bind(this, view);

        place.setText(consultationModelList.get(position));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, KnowBlankActivity.class);
                intent.putExtra("index", position);
                context.startActivity(intent);*/

            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @OnClick
    void onClick(View view){
        System.out.println();
    }

}
