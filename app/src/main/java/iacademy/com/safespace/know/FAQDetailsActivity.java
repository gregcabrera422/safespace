package iacademy.com.safespace.know;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.List;

public class FAQDetailsActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private View headerview;

    @BindView(R.id.know_nav_view)
    NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqdetails);

        ((TextView) findViewById(R.id.know_FAQ_question_textview)).setTypeface(FontHelper.BOLD(this));
        ((TextView) findViewById(R.id.know_FAQ_answer_textview)).setTypeface(FontHelper.REGULAR(this));

        int position = getIntent().getIntExtra("faqID", 0);
        ButterKnife.bind(this);
        drawerLayout = findViewById(R.id.know_faq_drawer_layout);

        List<String> questions = Arrays.asList(getResources().getStringArray(R.array.know_faq_questions));
        List<String> answers = Arrays.asList(getResources().getStringArray(R.array.know_faq_answers));

        TextView questionTextView = findViewById(R.id.know_FAQ_question_textview);
        TextView answerTextView = findViewById(R.id.know_FAQ_answer_textview);

        questionTextView.setText(questions.get(position));
        answerTextView.setText(answers.get(position));


        //handle onClick of nav Drawer
        headerview = navView.getHeaderView(0);

        headerview.findViewById(R.id.nav_mental_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(FAQDetailsActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "mental");
                startActivity(intent);
            }
        });

        headerview.findViewById(R.id.nav_consultation_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                startActivity(new Intent(FAQDetailsActivity.this, ConsultationCentersActivity.class));
            }
        });

        headerview.findViewById(R.id.nav_crisis_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(FAQDetailsActivity.this, KnowBlankActivity.class);
                intent.putExtra("listType", "crisis");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.RIGHT)){
            drawerLayout.closeDrawers();
        } else{
            super.onBackPressed();
        }
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    void onHamburgerClicked(){
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    public void setHamburgerNavFont(){
        ((TextView)headerview.findViewById(R.id.nav_factsheet_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_mental_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_consultation_textView)).setTypeface(FontHelper.BOLD(this));
        ((TextView)headerview.findViewById(R.id.nav_crisis_textView)).setTypeface(FontHelper.BOLD(this));
    }
}
