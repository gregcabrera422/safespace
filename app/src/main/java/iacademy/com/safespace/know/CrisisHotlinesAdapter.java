package iacademy.com.safespace.know;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class CrisisHotlinesAdapter extends RecyclerView.Adapter<CrisisHotlinesAdapter.ViewHolder> {

    private List<String> hotlineName;
    private List<String> hotlineDescription;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    CrisisHotlinesAdapter(Context context, List<String> hotlineName, List<String> hotlineDescription) {
        this.mInflater = LayoutInflater.from(context);
        this.hotlineName = hotlineName;
        this.hotlineDescription = hotlineDescription;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.crisis_hotlines_and_consultation_item_view, parent, false);
        ((TextView)view.findViewById(R.id.crisis_hotline_name)).setTypeface(FontHelper.BOLD(context));
        ((TextView)view.findViewById(R.id.crisis_hotline_description)).setTypeface(FontHelper.REGULAR(context));
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String name = hotlineName.get(position);
        String description = hotlineDescription.get(position);
        holder.name.setText(name);
        holder.description.setText(description);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return hotlineName.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView description;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.crisis_hotline_name);
            description = itemView.findViewById(R.id.crisis_hotline_description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return hotlineName.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}