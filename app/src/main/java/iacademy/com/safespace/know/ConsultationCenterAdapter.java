package iacademy.com.safespace.know;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class ConsultationCenterAdapter extends RecyclerView.Adapter<ConsultationCenterAdapter.ViewHolder> {

    private List<String> centerName;
    private List<String> centerDescription;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    ConsultationCenterAdapter(Context context, List<String> centerName, List<String> centerDescription) {
        this.mInflater = LayoutInflater.from(context);
        this.centerName = centerName;
        this.centerDescription = centerDescription;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.crisis_hotlines_and_consultation_item_view, parent, false);
        ((TextView)view.findViewById(R.id.crisis_hotline_name)).setTypeface(FontHelper.BOLD(context));
        ((TextView)view.findViewById(R.id.crisis_hotline_description)).setTypeface(FontHelper.REGULAR(context));
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String name = centerName.get(position);
        String description = centerDescription.get(position);
        holder.name.setText(name);
        holder.description.setText(description);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return centerName.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView description;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.crisis_hotline_name);
            description = itemView.findViewById(R.id.crisis_hotline_description);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return centerName.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}