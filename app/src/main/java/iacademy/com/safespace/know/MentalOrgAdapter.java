package iacademy.com.safespace.know;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class MentalOrgAdapter extends RecyclerView.Adapter<MentalOrgAdapter.ViewHolder> {

    private List<String> orgName;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    MentalOrgAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.orgName = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.mental_org_item_view, parent, false);
        ((TextView)view.findViewById(R.id.mental_org_name)).setTypeface(FontHelper.REGULAR(context));
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String question = orgName.get(position);
        holder.myTextView.setText(question);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return orgName.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.mental_org_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return orgName.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}