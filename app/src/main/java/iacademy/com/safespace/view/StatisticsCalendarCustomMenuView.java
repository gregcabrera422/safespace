package iacademy.com.safespace.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class StatisticsCalendarCustomMenuView extends Dialog {

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.legend_text)
    TextView legend_text;

    @BindView(R.id.sad)
    TextView sad;

    @BindView(R.id.down)
    TextView down;

    @BindView(R.id.happy)
    TextView happy;

    @BindView(R.id.annoyed)
    TextView annoyed;

    @BindView(R.id.angry)
    TextView angry;

    Context context;

    public StatisticsCalendarCustomMenuView(Context context) {
        super(context);
        this.context = context;
        //makes background transparent
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //customizes dialog position and removes dim on background
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.statistics_calendar_custom_menu_layout);
        ButterKnife.bind(this);
        text.setTypeface(FontHelper.REGULAR(context));
        legend_text.setTypeface(FontHelper.BOLD(context));
        sad.setTypeface(FontHelper.REGULAR(context));
        down.setTypeface(FontHelper.REGULAR(context));
        happy.setTypeface(FontHelper.REGULAR(context));
        annoyed.setTypeface(FontHelper.REGULAR(context));
        angry.setTypeface(FontHelper.REGULAR(context));

    }

}
