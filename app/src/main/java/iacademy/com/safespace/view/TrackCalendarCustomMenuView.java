package iacademy.com.safespace.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;

public class TrackCalendarCustomMenuView extends Dialog {

    @BindView(R.id.text)
    TextView text;

    Context context;

    public TrackCalendarCustomMenuView(Context context) {
        super(context);
        this.context = context;
        //makes background transparent
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //customizes dialog position and removes dim on background
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.track_calendar_custom_menu_layout);
        ButterKnife.bind(this);
        text.setTypeface(FontHelper.REGULAR(context));
    }

}
