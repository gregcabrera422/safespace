package iacademy.com.safespace.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.settings.AboutActivity;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.settings.FAQSettingsActivity;
import iacademy.com.safespace.utility.FontHelper;

public class CustomMenuView extends Dialog {

    @BindView(R.id.edit_text_view)
    TextView editTv;

    @BindView(R.id.faq_text_view)
    TextView faqTv;

    @BindView(R.id.about_text_view)
    TextView aboutTv;

    Context context;

    public CustomMenuView(Context context) {
        super(context);
        this.context = context;
        //makes background transparent
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //customizes dialog position and removes dim on background
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.TOP|Gravity.RIGHT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_menu_layout);
        ButterKnife.bind(this);
        editTv.setTypeface(FontHelper.BOLD(context));
        faqTv.setTypeface(FontHelper.BOLD(context));
        aboutTv.setTypeface(FontHelper.BOLD(context));
    }

    public void showDialog(){
        show();
    }

    @OnClick(R.id.edit_button)
    public void editClicked(){
        context.startActivity(new Intent(context, AvatarActivity.class));
    }

    @OnClick(R.id.faq_button)
    public void faqClicked(){
        context.startActivity(new Intent(context, FAQSettingsActivity.class));


    }

    @OnClick(R.id.about_button)
    public void aboutClicked(){
        context.startActivity(new Intent(context, AboutActivity.class));

    }

}
