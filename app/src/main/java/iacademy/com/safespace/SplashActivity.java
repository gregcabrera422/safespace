package iacademy.com.safespace;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.moodcheckin.CheckInActivity1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, CheckInActivity1.class));
                finish();
            }
        }, secondsDelayed * 1000);
    }
}
