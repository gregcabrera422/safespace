package iacademy.com.safespace;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.home.HomeAdapter;
import iacademy.com.safespace.model.AvatarModel;
import iacademy.com.safespace.model.AvatarPartsModel;
import iacademy.com.safespace.settings.AvatarActivity;
import iacademy.com.safespace.utility.Assets;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;
import iacademy.com.safespace.view.CustomMenuView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.avatar_imageview)
    ImageView avatar_imageview;

    @BindView(R.id.background)
    RelativeLayout background;

    @BindView(R.id.home_maintext_textview)
    TextView mainText;

    @BindView(R.id.home_subtext_textview)
    TextView subText;

    @BindView(R.id.home_item_viewpager)
    ViewPager homeItemViewPager;

    private HomeAdapter homeAdapter;

    private String name;

    private CustomMenuView customMenuView;

    private AvatarModel avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainText.setTypeface(FontHelper.BOLD(this));
        subText.setTypeface(FontHelper.BOLD(this));

        final float scale = MainActivity.this.getResources().getDisplayMetrics().density;
        int dps = 65;

        int pixels = (int) (dps * scale + 0.5f);
        homeItemViewPager.setPadding(pixels,0, pixels,0);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSharedPrefs();
    }

    public void initView(){
        homeAdapter = new HomeAdapter(this);
        homeItemViewPager.setAdapter(homeAdapter);

        homeItemViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch(position){
                    case 0:
                        background.setBackgroundResource(R.drawable.homepage_track_bg);
                        break;
                    case 1:
                        background.setBackgroundResource(R.drawable.homepage_create_bg);
                        break;
                    case 2:
                        background.setBackgroundResource(R.drawable.homepage_meditate_bg);
                        break;
                    case 3:
                        background.setBackgroundResource(R.drawable.homepage_know_bg);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        getSharedPrefs();
    }

    public void getSharedPrefs(){
        name = new SharedPreferencesHelper(this).getSharedPrefs.getString("Name", null);
        mainText.setText(String.format(getResources().getString(R.string.welcome_maintext_label), name));

        //set avatar
        Gson gson = new Gson();
        String json = new SharedPreferencesHelper(this).getSharedPrefs.getString("avatar", null);
        avatar = new AvatarModel(this);
        if (json == null){
            avatar = new AvatarModel(this);
            avatar.setEyes(Assets.getEyes().get(0));
            avatar.setHair(Assets.getHair().get(0));
            avatar.setLips(Assets.getLips().get(0));
            avatar.setNose(Assets.getNose().get(0));
            avatar.setShirt(Assets.getShirts().get(0));
            avatar.setSkin(Assets.getSkins().get(0));

            avatar_imageview.setImageBitmap(avatar.generateAvatar());
        }else{
            Type type = new TypeToken<List<AvatarPartsModel>>(){}.getType();
            avatar.parseArrayList(gson.fromJson(json, type));
            avatar_imageview.setImageBitmap(avatar.generateAvatar());
        }
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        customMenuView = new CustomMenuView( this);
        customMenuView.showDialog();
    }

}
