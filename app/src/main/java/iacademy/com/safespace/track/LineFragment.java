package iacademy.com.safespace.track;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.MoodDataHelper;

public class LineFragment extends Fragment {

    @BindView(R.id.chart)
    LineChart chart;

    private ArrayList<ILineDataSet> dataSets = null;
    private ArrayList<Integer> colors;

    private ArrayList<Entry> emotionEntry;

    private ArrayList<Integer> values;

    private LineData data;

    public LineFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_line, container, false);
        ButterKnife.bind(this, view);

        colors = new ArrayList<Integer>();
        emotionEntry = new ArrayList<>();
        data = null;
        values = MoodDataHelper.getMoodsForLineGraph(getActivity());

        if(values!=null && values.size() != 0) {
            data = new LineData(getDataSet());
            chart.setData(data);
            chart.fitScreen();
            setXAxis(chart);
            setYAxis(chart);
            setExtraDesign();
            chart.invalidate();
        }

        return view;
    }

    //functions that input data on linegraph
    private ArrayList<ILineDataSet> getDataSet() {
        int day = 0;
        for(int value : values) {
            // first parameter (day, value)
            emotionEntry.add(new Entry(day++, value));

            if (value >= 5)
                // value > 4 displays blue
                colors.add(getResources().getColor(R.color.sad_color));
            else if (value >= 4)
                // value > 4 displays purple
                colors.add(getResources().getColor(R.color.down_color));
            else if (value >= 3)
                // value > 4 displays green
                colors.add(getResources().getColor(R.color.happy_color));
            else if (value >= 2)
                // value > 4 displays orange
                colors.add(getResources().getColor(R.color.annoyed_color));
            else
                // value > 4 displays red
                colors.add(getResources().getColor(R.color.angry_color));
        }

        LineDataSet lineDataSet = new LineDataSet(emotionEntry, "Emotion");
        lineDataSet.setColor(R.color.black);
        lineDataSet.setCircleColors(colors);
        lineDataSet.setDrawValues(false);
        lineDataSet.setCircleRadius(8f);
        lineDataSet.setLineWidth(2);
        lineDataSet.setDrawCircleHole(false);
        lineDataSet.setValueTextSize(20f);
        dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);
        return dataSets;
    }

    private void setXAxis(LineChart chart){
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(getResources().getColor(R.color.black));
        xAxis.setLabelCount(4);
        xAxis.setAxisMaxValue(31);
        xAxis.setAxisMinValue(0);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
    }

    private void setYAxis(LineChart chart) {
        YAxis yAxis = chart.getAxisLeft();
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setEnabled(false);
        yAxis.setLabelCount(5);
        yAxis.setAxisMaxValue(5);
        yAxis.setAxisMinValue(0);
    }

    private void setExtraDesign() {
        chart.getDescription().setEnabled(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.setScaleEnabled(false);
        chart.getLegend().setTextColor(R.color.black);
        chart.setBorderColor(255);
        chart.setDrawGridBackground(false);
        chart.setBackgroundColor(getResources().getColor(R.color.white));
        chart.getLegend().setEnabled(false);
        chart.setDragEnabled(false);
    }


}
