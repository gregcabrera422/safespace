package iacademy.com.safespace.track;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.ViewPager;
import iacademy.com.safespace.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticFragment extends Fragment {
    ViewPager pager;
    View view;

    public StatisticFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_statistic, container, false);
        pager = view.findViewById(R.id.pager);
        StatisticsAdapter adapter = new StatisticsAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);

        return view;
    }

}
