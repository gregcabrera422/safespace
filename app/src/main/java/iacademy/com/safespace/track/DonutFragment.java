package iacademy.com.safespace.track;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.MoodDataHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class DonutFragment extends Fragment {
    View view;
    FitChart donut;
    ArrayList<FitChartValue> values;
    HashMap<String, Integer> moods;
    SharedPreferences s;
    Gson gson;
    TextView sad;
    TextView down;
    TextView happy;
    TextView annoyed;
    TextView angry;
    ImageView middle_img;

    public DonutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_donut, container, false);
        donut = (FitChart) view.findViewById(R.id.donut);
        sad = (TextView)view.findViewById(R.id.sad);
        down = (TextView)view.findViewById(R.id.down);
        happy = (TextView)view.findViewById(R.id.happy);
        annoyed = (TextView)view.findViewById(R.id.annoyed);
        angry = (TextView)view.findViewById(R.id.angry);
        middle_img = (ImageView)view.findViewById(R.id.prominent);
        moods = MoodDataHelper.getAllMoods(getActivity());

        if(moods!=null) {
            setupDonut();
            updateImage();
        }
        return view;
    }

    public void setupDonut(){
        int max = 0;
        for (Integer num: moods.values()){
            max += num;
        }

        donut.setMaxValue(max);
        values = new ArrayList<FitChartValue>();
        values.add(new FitChartValue(moods.get("sad"), Color.parseColor("#4FACFF")));
        values.add(new FitChartValue(moods.get("down"), Color.parseColor("#9E63E9")));
        values.add(new FitChartValue(moods.get("happy"), Color.parseColor("#39CE70")));
        values.add(new FitChartValue(moods.get("annoyed"), Color.parseColor("#EF8D2F")));
        values.add(new FitChartValue(moods.get("angry"), Color.parseColor("#E31328")));
        donut.setValues(values);

        if (max != 0){
            DecimalFormat df = new DecimalFormat("#");
            sad.setText(df.format((float)moods.get("sad")/max*100)+"%");
            down.setText(df.format((float)moods.get("down")/max*100) + "%");
            happy.setText(df.format((float)moods.get("happy")/max*100) + "%");
            annoyed.setText(df.format((float)moods.get("annoyed")/max*100) + "%");
            angry.setText(df.format((float)moods.get("angry")/max*100) + "%");
        }
    }

    public void updateImage(){
        int highest = Collections.max(moods.values());
        for (Map.Entry<String, Integer> entry : moods.entrySet()) {
            if (entry.getValue()==highest) {
                switch (entry.getKey()){
                    case "sad":
                        middle_img.setBackgroundResource(R.drawable.sad);
                        break;
                    case "down":
                        middle_img.setBackgroundResource(R.drawable.down);
                        break;
                    case "happy":
                        middle_img.setBackgroundResource(R.drawable.happy);
                        break;
                    case "annoyed":
                        middle_img.setBackgroundResource(R.drawable.annoyed);
                        break;
                    case "angry":
                        middle_img.setBackgroundResource(R.drawable.angry);
                        break;
                }
            }
        }
    }

}
