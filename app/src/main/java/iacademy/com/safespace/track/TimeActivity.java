package iacademy.com.safespace.track;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.moodcheckin.CheckInHomeActivity;
import iacademy.com.safespace.moodcheckin.FeelingBetterActivity;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.ScheduledService;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Arrays;

public class TimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        String date = getIntent().getStringExtra("date");

        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        ImageButton confirmBtn = findViewById(R.id.confirmBtn);
        confirmBtn.setEnabled(false);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send out notification after 10 seconds
                int seconds = 10;
                AlarmManager mgr = (AlarmManager) TimeActivity.this.getSystemService(Context.ALARM_SERVICE);
                Intent i = new Intent(TimeActivity.this, ScheduledService.class);
                PendingIntent pi = PendingIntent.getService(TimeActivity.this, 0, i, 0);
                mgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (seconds * 1000), pi);

                boolean isSkipClicked = new SharedPreferencesHelper(TimeActivity.this).getSharedPrefs.getBoolean("isSkipClicked", false);
                if (!isSkipClicked){
                    new SharedPreferencesHelper(TimeActivity.this).Write("isSkipClicked", true);
                    Intent intent = new Intent(TimeActivity.this, FeelingBetterActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else{
                    finish();
                }
            }
        });

        /*TextView spinnerText = findViewById(R.id.spinnerText1);
        spinnerText.setTypeface(FontHelper.BOLD(this));
        CheckedTextView dropdownItem = findViewById(R.id.dropdown_item);
        dropdownItem.setTypeface(FontHelper.BOLD(this));*/

        findViewById(R.id.dropdown_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.performClick();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, Arrays.asList(getResources().getStringArray(R.array.time)))
        {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(FontHelper.BOLD(TimeActivity.this));

/*                final float scale = TimeActivity.this.getResources().getDisplayMetrics().density;
                int dps = 20;

                int pixels = (int) (dps * scale + 0.5f);
                v.setPadding(0, pixels, 0, pixels);*/
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(FontHelper.BOLD(TimeActivity.this));

                final float scale = TimeActivity.this.getResources().getDisplayMetrics().density;
                int dps = 20;

                int pixels = (int) (dps * scale + 0.5f);
                v.setPadding(0, pixels, 0, pixels);
                return v;
            }
        };

        /*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time, R.layout.spinner_layout){

        };*/
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_layout);

        spinner.setAdapter(adapter);
        spinner.setDropDownWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        spinner.setVerticalScrollbarPosition(View.SCROLLBAR_POSITION_LEFT);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    confirmBtn.setEnabled(false);
                    confirmBtn.setVisibility(View.INVISIBLE);
                }else {
                    confirmBtn.setEnabled(true);
                    confirmBtn.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //setHeight of dropdown
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);

            final float scale = this.getResources().getDisplayMetrics().density;
            int dps = 180;

            int pixels = (int) (dps * scale + 0.5f);
            popupWindow.setHeight(pixels);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
    }
}
