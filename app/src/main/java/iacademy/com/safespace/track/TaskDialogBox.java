package iacademy.com.safespace.track;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import iacademy.com.safespace.R;

public class TaskDialogBox extends Dialog implements android.view.View.OnClickListener{
    Context context;
    Button high;
    Button normal;
    Button low;
    Button confirm;
    int status = -1;
    TextView task;
    EditText taskName;
    String input = "";
    public TaskDialogBox(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.task_dialog);
        high = (Button)findViewById(R.id.high);
        normal = (Button)findViewById(R.id.normal);
        low = (Button)findViewById(R.id.low);
        confirm = (Button)findViewById(R.id.confirm);
        task = (TextView)((Activity)context).findViewById(R.id.tasks);
        taskName = (EditText)findViewById(R.id.task);
        high.setOnClickListener(this);
        normal.setOnClickListener(this);
        low.setOnClickListener(this);
        confirm.setOnClickListener(this);
        taskName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                input = s.toString();
                check();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        high.setBackgroundResource(R.drawable.urgent_priority_btn);
        normal.setBackgroundResource(R.drawable.normal_priority_btn);
        low.setBackgroundResource(R.drawable.low_priority_btn);
        switch (v.getId()){
            case R.id.high:
                status = R.drawable.urgent_priority_btn;
                high.setBackgroundResource(R.drawable.urgent_active);
                break;
            case R.id.normal:
                status = R.drawable.normal_priority_btn;
                normal.setBackgroundResource(R.drawable.priority_active);
                break;
            case R.id.low:
                status = R.drawable.low_priority_btn;
                low.setBackgroundResource(R.drawable.low_priority_active);
                break;
            case R.id.confirm:
                update();
                dismiss();
                break;
        }
        check();
    }

    public void update(){
        task.setCompoundDrawablesWithIntrinsicBounds(status,0,0,0);
        task.setText(taskName.getText().toString());
        task.setTag(status);
    }

    public void check(){
        if (status != -1 && input.length() > 0){
            confirm.setVisibility(View.VISIBLE);
        }else{
            confirm.setVisibility(View.INVISIBLE);
        }
    }
}
