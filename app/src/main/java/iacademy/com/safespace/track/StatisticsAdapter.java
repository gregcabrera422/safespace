package iacademy.com.safespace.track;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class StatisticsAdapter extends FragmentPagerAdapter {

    public StatisticsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new DonutFragment();
            case 1: return  new LineFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Donut";
            case 1:
                return "Line";
        }
        return null;
    }

}
