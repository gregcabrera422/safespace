package iacademy.com.safespace.track;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import iacademy.com.safespace.R;

public class ThoughtDialogBox extends Dialog implements android.view.View.OnClickListener{
    Context context;
    EditText thought_entry;
    TextView thought;
    Button confirm;
    public ThoughtDialogBox(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.thought_dialog);
        thought_entry = (EditText)findViewById(R.id.thought);
        thought = (TextView)((Activity)context).findViewById(R.id.thought);
        confirm = (Button)findViewById(R.id.confirm);
        confirm.setOnClickListener(this);
        thought_entry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0){
                    confirm.setVisibility(View.VISIBLE);
                }else{
                    confirm.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                thought.setText(thought_entry.getText().toString());
                dismiss();
                break;
        }
    }
}
