package iacademy.com.safespace.track;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import iacademy.com.safespace.R;
import iacademy.com.safespace.model.MoodModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {
    CompactCalendarView calendar;
    TextView month;
    TextView year;
    Calendar currentTime;

    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        currentTime = Calendar.getInstance();
        year = (TextView)getActivity().findViewById(R.id.year);
        month = (TextView)getActivity().findViewById(R.id.month);
        calendar = view.findViewById(R.id.calendar);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.shouldDrawIndicatorsBelowSelectedDays(true);
        addEvents();
        update();
        calendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                currentTime.setTime(dateClicked);
                Log.v("", calendar.getEvents(dateClicked.getTime())+"");
                Intent intent = new Intent(getActivity(), MoodActivity.class);
                intent.putExtra("date", new SimpleDateFormat("MMMM dd").format(currentTime.getTime()));
                intent.putExtra("time", dateClicked.getTime());
                Log.v("date", dateClicked.getTime()+"");
                startActivity(intent);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                currentTime.setTime(firstDayOfNewMonth);
                update();
            }
        });
        return view;
    }

    public void update(){
        month.setText(new SimpleDateFormat("MMMM").format(currentTime.getTime()));
        year.setText(new SimpleDateFormat("yyyy").format(currentTime.getTime()));
    }

    public void addEvents(){
        calendar.removeAllEvents();
        SharedPreferences s = getActivity().getApplicationContext().getSharedPreferences("dates", Context.MODE_PRIVATE);
        Type type = new TypeToken<List<MoodModel>>(){}.getType();
        Gson gson = new Gson();
        Event event1;
        Map<String, ?> events = s.getAll();
        for (Map.Entry<String, ?> event: events.entrySet()){
            ArrayList<MoodModel> entries = gson.fromJson(event.getValue().toString(), type);
            for (MoodModel entry : entries){
                switch (entry.getTask_img()){
                    case R.drawable.low_priority_btn:
                        event1 = new Event(getResources().getColor(R.color.low_priority), Long.parseLong(event.getKey()));
                        calendar.addEvent(event1);
                        break;
                    case R.drawable.normal_priority_btn:
                        event1 = new Event(getResources().getColor(R.color.normal_priority), Long.parseLong(event.getKey()));
                        calendar.addEvent(event1);
                        break;
                    case R.drawable.urgent_priority_btn:
                        event1 = new Event(getResources().getColor(R.color.high_priority), Long.parseLong(event.getKey()));
                        calendar.addEvent(event1);
                        break;
                }
            }

        }
    }

    @Override
    public void onResume() {
        addEvents();
        super.onResume();
    }
}
