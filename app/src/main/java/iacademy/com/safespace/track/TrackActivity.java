package iacademy.com.safespace.track;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.track.CalendarFragment;
import iacademy.com.safespace.track.StatisticFragment;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.view.StatisticsCalendarCustomMenuView;
import iacademy.com.safespace.view.TrackCalendarCustomMenuView;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TrackActivity extends AppCompatActivity {
    FragmentManager fm;

    @BindView(R.id.calendar)
    TextView calendar;

    @BindView(R.id.statistics)
    TextView statistics;

    @BindView(R.id.month)
    TextView month;

    @BindView(R.id.year)
    TextView year;

    Fragment f;

    private TrackCalendarCustomMenuView trackCalendarCustomMenuView;

    private StatisticsCalendarCustomMenuView statisticsCalendarCustomMenuView;

    int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        ButterKnife.bind(this);
        calendar.setTypeface(FontHelper.BOLD(this));
        statistics.setTypeface(FontHelper.BOLD(this));
        month.setTypeface(FontHelper.BOLD(this));
        year.setTypeface(FontHelper.BOLD(this));

        fm = getSupportFragmentManager();
        f = new CalendarFragment();
        fm.beginTransaction().replace(R.id.container, f).commit();
    }

    public void changeTab(View view){
        switch (view.getId()){
            case R.id.calendar:
                f = new CalendarFragment();
                fm.beginTransaction().replace(R.id.container, f).commit();
                calendar.setTextSize(25);
                calendar.setTextColor(getResources().getColor(R.color.white));
                statistics.setTextSize(15);
                statistics.setTextColor(getResources().getColor(R.color.calendar_inactive));
                page = 0;
                break;
            case R.id.statistics:
                f = new StatisticFragment();
                fm.beginTransaction().replace(R.id.container, f).commit();
                calendar.setTextSize(15);
                calendar.setTextColor(getResources().getColor(R.color.calendar_inactive));
                statistics.setTextSize(25);
                statistics.setTextColor(getResources().getColor(R.color.white));
                page = 1;
                break;
        }
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        if(page==0) {
            trackCalendarCustomMenuView = new TrackCalendarCustomMenuView(this);
            trackCalendarCustomMenuView.show();
        } else {
            statisticsCalendarCustomMenuView = new StatisticsCalendarCustomMenuView(this);
            statisticsCalendarCustomMenuView.show();
        }
    }

}
