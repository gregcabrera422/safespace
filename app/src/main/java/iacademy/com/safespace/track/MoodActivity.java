package iacademy.com.safespace.track;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iacademy.com.safespace.R;
import iacademy.com.safespace.model.MoodModel;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.view.EntryCustomMenuView;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MoodActivity extends AppCompatActivity {
    TextView date;
    TextView thought;
    TextView tasks;
    TextView maintext;
    Button taskBtn;
    ArrayList<ImageButton> moods;
    Button confirm;
    String mood;
    int pointer = -1;

    private EntryCustomMenuView entryCustomMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mood);
        ButterKnife.bind(this);

        thought = (TextView)findViewById(R.id.thought);
        tasks = (TextView)findViewById(R.id.tasks);
        taskBtn = (Button)findViewById(R.id.task_btn);
        confirm = (Button)findViewById(R.id.confirm);
        moods = new ArrayList<ImageButton>();
        moods.add((ImageButton)findViewById(R.id.sad));
        moods.add((ImageButton)findViewById(R.id.down));
        moods.add((ImageButton)findViewById(R.id.happy));
        moods.add((ImageButton)findViewById(R.id.annoyed));
        moods.add((ImageButton)findViewById(R.id.angry));

        date = (TextView)findViewById(R.id.date);
        date.setTypeface(FontHelper.BOLD(this));
        date.setText(getIntent().getStringExtra("date"));

        maintext = findViewById(R.id.maintext);
        maintext.setTypeface(FontHelper.REGULAR(this));

    }

    public void changeMood(View view){
        if (pointer != -1){
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
            moods.get(pointer).startAnimation(animation);
        }
        switch (view.getId()){
            case R.id.sad:
                enlarge(0);
                mood = "sad";
                break;
            case R.id.down:
                enlarge(1);
                mood = "down";
                break;
            case R.id.happy:
                enlarge(2);
                mood = "happy";
                break;
            case R.id.annoyed:
                enlarge(3);
                mood = "annoyed";
                break;
            case R.id.angry:
                enlarge(4);
                mood = "angry";
                break;
        }
        check();
    }

    public void enlarge(int position){
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        moods.get(position).startAnimation(animation);
        pointer = position;
    }

    public void openDialog(View view){
        switch (view.getId()){
            case R.id.thought_btn:
                ThoughtDialogBox dialogBox = new ThoughtDialogBox(this);
                dialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogBox.show();
                dialogBox.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        check();
                    }
                });
                break;
            case R.id.task_btn:
                TaskDialogBox dialogBox1 = new TaskDialogBox(this);
                dialogBox1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogBox1.show();
                dialogBox1.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        check();
                    }
                });
                break;
        }
    }

    public void check(){
        if (thought.getText().toString().length() > 0 && tasks.getText().toString().length() > 0 && pointer != -1){
            confirm.setVisibility(View.VISIBLE);
        }else{
            confirm.setVisibility(View.INVISIBLE);
        }
    }

    public void complete(View view){
        //save data
        SharedPreferences s = getApplicationContext().getSharedPreferences("dates", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = s.edit();
        String entry = s.getString(getIntent().getLongExtra("time", 0L)+"", null);
        MoodModel m = new MoodModel();
        Gson gson = new Gson();
        ArrayList<MoodModel> entries;
        if (entry != null){
            Type type = new TypeToken<List<MoodModel>>(){}.getType();
            entries = gson.fromJson(entry, type);
        }else{
            entries = new ArrayList<>();
        }
        m.setTask(tasks.getText().toString());
        m.setTask_img((int)tasks.getTag());
        m.setThough(thought.getText().toString());
        m.setMood(mood);
        entries.add(m);
        editor.putString(getIntent().getLongExtra("time", 0L)+"", gson.toJson(entries));
        editor.apply();

        Intent intent = new Intent(MoodActivity.this, TimeActivity.class);
        intent.putExtra("date", date.getText().toString());
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.back_btn)
    void onBackBtnClicked(){
        onBackPressed();
        finish();
    }

    @OnClick(R.id.hamburger_btn)
    public void hamburgerClicked(){
        entryCustomMenuView = new EntryCustomMenuView(this);
        entryCustomMenuView.show();
    }
}
