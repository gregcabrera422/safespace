package iacademy.com.safespace.moodcheckin;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.MainActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.know.KnowActivity;
import iacademy.com.safespace.know.KnowBlankActivity;
import iacademy.com.safespace.utility.FontHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FeelingBetterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeling_better);

        TextView title = findViewById(R.id.feeling_better_title);
        TextView desc1 = findViewById(R.id.feeling_better_desc1);
        TextView desc2 = findViewById(R.id.feeling_better_desc2);

        title.setTypeface(FontHelper.BOLD(this));
        desc1.setTypeface(FontHelper.REGULAR(this));
        desc2.setTypeface(FontHelper.REGULAR(this));

        findViewById(R.id.learnMoreBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FeelingBetterActivity.this, KnowActivity.class));
            }
        });

        findViewById(R.id.exploreBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeelingBetterActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }
}
