package iacademy.com.safespace.moodcheckin;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;


public class CheckInActivity2 extends AppCompatActivity {
    PopupWindow mPopupWindow = null;
    LinearLayout checkIn_nextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in2);

        ((TextView)findViewById(R.id.welcome2_title)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.welcome2_description)).setTypeface(FontHelper.REGULAR(this));

        checkIn_nextBtn = findViewById(R.id.checkIn2_nextBtn);
        EditText name = (EditText) findViewById(R.id.checkIn2_nameEditText);

        //handle enter keypress
        name.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                //If the keyevent is a key-down event on the "enter" button
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(name.getText().toString().length() <= 0){
                        name.setError("Please enter your name.");
                    }else {
                        checkIn_nextBtn.setEnabled(false);
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        // Inflate the custom layout/view
                        View customView = inflater.inflate(R.layout.checkin_popup_avatar,null);
                        // Initialize a new instance of popup window
                        mPopupWindow = new PopupWindow(
                                customView,
                                LayoutParams.MATCH_PARENT,
                                LayoutParams.MATCH_PARENT
                        );

                        // Set an elevation value for popup window
                        // Call requires API level 21
                        if(Build.VERSION.SDK_INT>=21){
                            mPopupWindow.setElevation(5.0f);
                        }

                        // Get a reference for the custom view close button
                        FrameLayout pop_up_layout = (FrameLayout) customView.findViewById(R.id.check_in_pop_up_avatar_layout);

                        // Set a click listener for the popup window close button
                        pop_up_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Dismiss the popup window
                                //store inputted name to sharedprefs
                                new SharedPreferencesHelper(CheckInActivity2.this).Write("Name", name.getText().toString());

                                mPopupWindow.dismiss();
                                startActivity(new Intent(CheckInActivity2.this, CheckInHomeActivity.class));
                                finish();
                            }
                        });
                        // Finally, show the popup window at the center location of root relative layout
                        mPopupWindow.showAtLocation(findViewById(R.id.checkIn2_framelayout), Gravity.CENTER,0,0);
                    }
                    return true;
                }
                return false;
            }
        });

        //handle next button
        checkIn_nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().length() <= 0){
                    name.setError("Please enter your name.");
                }else {
                    checkIn_nextBtn.setEnabled(false);
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                    // Inflate the custom layout/view
                    View customView = inflater.inflate(R.layout.checkin_popup_avatar,null);
                    // Initialize a new instance of popup window
                    mPopupWindow = new PopupWindow(
                            customView,
                            LayoutParams.MATCH_PARENT,
                            LayoutParams.MATCH_PARENT
                    );


                    // Set an elevation value for popup window
                    // Call requires API level 21
                    if(Build.VERSION.SDK_INT>=21){
                        mPopupWindow.setElevation(5.0f);
                    }

                    // Get a reference for the custom view close button
                    FrameLayout pop_up_layout = (FrameLayout) customView.findViewById(R.id.check_in_pop_up_avatar_layout);

                    // Set a click listener for the popup window close button
                    pop_up_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Dismiss the popup window
                            //store inputted name to sharedprefs
                            new SharedPreferencesHelper(CheckInActivity2.this).Write("Name", name.getText().toString());

                            mPopupWindow.dismiss();
                            startActivity(new Intent(CheckInActivity2.this, CheckInHomeActivity.class));
                            finish();
                        }
                    });
                    // Finally, show the popup window at the center location of root relative layout
                    mPopupWindow.showAtLocation(findViewById(R.id.checkIn2_framelayout), Gravity.CENTER,0,0);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(mPopupWindow != null){
            mPopupWindow.dismiss();
            mPopupWindow = null;
            checkIn_nextBtn.setEnabled(true);
        }else {
            super.onBackPressed();
        }
    }
}
