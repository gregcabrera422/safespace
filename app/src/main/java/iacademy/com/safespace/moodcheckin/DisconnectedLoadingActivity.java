package iacademy.com.safespace.moodcheckin;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.MainActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.create.CreateActivity;
import iacademy.com.safespace.track.TrackActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class DisconnectedLoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disconnected_loading);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                startActivity(new Intent(DisconnectedLoadingActivity.this, TrackActivity.class));
                finish();
            }
        }, 2000);
    }
}
