package iacademy.com.safespace.moodcheckin;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.MainActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.meditate.MeditateActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class WorriedLoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worried_loading);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                startActivity(new Intent(WorriedLoadingActivity.this, MeditateActivity.class));
                finish();
            }
        }, 2000);
    }
}
