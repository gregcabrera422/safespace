package iacademy.com.safespace.moodcheckin;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

public class CheckInActivity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in1);

        ((TextView)findViewById(R.id.welcome_title)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.welcome_description)).setTypeface(FontHelper.REGULAR(this));

        //check if opening for the first time
        String name = new SharedPreferencesHelper(CheckInActivity1.this).getSharedPrefs.getString("Name", null);
        if(name != null){
            startActivity(new Intent(CheckInActivity1.this, CheckInHomeActivity.class));
            finish();
        }

        LinearLayout checkIn_nextBtn = findViewById(R.id.checkIn1_nextBtn);
        checkIn_nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckInActivity1.this, CheckInActivity2.class));
                finish();
            }
        });
    }
}
