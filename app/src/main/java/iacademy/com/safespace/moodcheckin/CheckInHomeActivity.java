package iacademy.com.safespace.moodcheckin;

import androidx.appcompat.app.AppCompatActivity;
import iacademy.com.safespace.MainActivity;
import iacademy.com.safespace.R;
import iacademy.com.safespace.utility.FontHelper;
import iacademy.com.safespace.utility.SharedPreferencesHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CheckInHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_home);

        ((TextView)findViewById(R.id.checkInHome_greetings)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.checkInHome_greetings_description)).setTypeface(FontHelper.REGULAR(this));
        /*((TextView)findViewById(R.id.checkin_home1_title)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.checkin_home1_description)).setTypeface(FontHelper.REGULAR(this));
        ((TextView)findViewById(R.id.checkin_home2_title)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.checkin_home2_description)).setTypeface(FontHelper.REGULAR(this));
        ((TextView)findViewById(R.id.checkin_home3_title)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.checkin_home3_description)).setTypeface(FontHelper.REGULAR(this));*/
        ((TextView)findViewById(R.id.checkInHome_skipBtn1_tv)).setTypeface(FontHelper.BOLD(this));
        ((TextView)findViewById(R.id.checkInHome_skipBtn2_tv)).setTypeface(FontHelper.BOLD(this));

        //check if not opening for the first time
        String checker = new SharedPreferencesHelper(CheckInHomeActivity.this).getSharedPrefs.getString("Name", null);
        if(checker == null){
            startActivity(new Intent(CheckInHomeActivity.this, CheckInActivity1.class));
            finish();
        }

        //get name from shared prefs
        String name = new SharedPreferencesHelper(CheckInHomeActivity.this).getSharedPrefs.getString("Name", "...");
        ((TextView) findViewById(R.id.checkInHome_greetings)).setText("Hello, " + name + "!");

        //handle skip onClick
        findViewById(R.id.checkInHome_skipBtn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SharedPreferencesHelper(CheckInHomeActivity.this).Write("isSkipClicked", true);
                startActivity(new Intent(CheckInHomeActivity.this, MainActivity.class));
                finish();
            }
        });

        findViewById(R.id.checkInHome_skipBtn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SharedPreferencesHelper(CheckInHomeActivity.this).Write("isSkipClicked", true);
                startActivity(new Intent(CheckInHomeActivity.this, MainActivity.class));
                finish();
            }
        });

        ImageView stressBtn = findViewById(R.id.stress_btn);
        ImageView disconnectedBtn = findViewById(R.id.disconnected_btn);
        ImageView worriedBtn = findViewById(R.id.worried_btn);

        stressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stressBtn.getTag().equals("stressed_btn")){
                    stressBtn.setTag("stressed_activated");
                    disconnectedBtn.setTag("disconnected_btn");
                    worriedBtn.setTag("worried_btn");

                    stressBtn.setImageResource(R.drawable.stressed_activated);
                    disconnectedBtn.setImageResource(R.drawable.disconnected_btn);
                    worriedBtn.setImageResource(R.drawable.worried_btn);
                }else{
                    new SharedPreferencesHelper(CheckInHomeActivity.this).Write("isSkipClicked", false);
                    startActivity(new Intent(CheckInHomeActivity.this, StressedLoadingActivity.class));
                }
            }
        });

        disconnectedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(disconnectedBtn.getTag().equals("disconnected_btn")){
                    stressBtn.setTag("stressed_btn");
                    disconnectedBtn.setTag("disconnected_activated");
                    worriedBtn.setTag("worried_btn");

                    stressBtn.setImageResource(R.drawable.stressed_btn);
                    disconnectedBtn.setImageResource(R.drawable.disconnected_activated);
                    worriedBtn.setImageResource(R.drawable.worried_btn);
                }else{
                    new SharedPreferencesHelper(CheckInHomeActivity.this).Write("isSkipClicked", false);
                    startActivity(new Intent(CheckInHomeActivity.this, DisconnectedLoadingActivity.class));
                }
            }
        });

        worriedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(worriedBtn.getTag().equals("worried_btn")){
                    stressBtn.setTag("stressed_btn");
                    disconnectedBtn.setTag("disconnected_btn");
                    worriedBtn.setTag("worried_activated");

                    stressBtn.setImageResource(R.drawable.stressed_btn);
                    disconnectedBtn.setImageResource(R.drawable.disconnected_btn);
                    worriedBtn.setImageResource(R.drawable.worried_activated);
                }else{
                    new SharedPreferencesHelper(CheckInHomeActivity.this).Write("isSkipClicked", false);
                    startActivity(new Intent(CheckInHomeActivity.this, WorriedLoadingActivity.class));
                }
            }
        });
    }
}
