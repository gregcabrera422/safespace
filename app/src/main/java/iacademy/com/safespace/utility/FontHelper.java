package iacademy.com.safespace.utility;

import android.content.Context;
import android.graphics.Typeface;

import java.io.File;

public class FontHelper {

    public static Typeface BOLD(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-Bold.otf");
    }

    public static Typeface BOLD_ITALIC(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-BoldItalic.otf");
    }

    public static Typeface DASH(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-Dash.otf");
    }

    public static Typeface ITALIC(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-Italic.otf");
    }

    public static Typeface LIGHT(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-Light.otf");
    }

    public static Typeface LIGHT_ITALIC(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-LightItalic.otf");
    }

    public static Typeface REGULAR(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts" + File.separator + "Quicksand-Regular.otf");
    }
}
