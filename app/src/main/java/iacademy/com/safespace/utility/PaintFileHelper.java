package iacademy.com.safespace.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.RelativeLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import iacademy.com.safespace.create.PaintView;
import iacademy.com.safespace.model.ArtworkModel;

public class PaintFileHelper {

    private final String PAINTING_FILE_PATH = "/SafeSpace/paintings";
    private final String PAINTING_FILE_NAME_EXTENSION = ".jpg";

    Bitmap paintViewBitmap;

    Context context;

    PaintView paintView;

    RelativeLayout paintBackgroundView;

    public PaintFileHelper(Context context) {
        this.context = context;
    }

    public PaintFileHelper(Context context, PaintView paintView) {
        this.context = context;
        this.paintView = paintView;
    }

    public PaintFileHelper(Context context, RelativeLayout paintBackgroundView) {
        this.context = context;
        this.paintBackgroundView = paintBackgroundView;
    }

    public void saveImage(String fileName){
        paintViewBitmap = getBitmapFromView(paintView , paintView.getCurrentHeight(), paintView.getCurrentWidth());

        File folder = new File(Environment.getExternalStorageDirectory() + PAINTING_FILE_PATH);
        folder.mkdirs();

        File file = new File(folder, fileName + PAINTING_FILE_NAME_EXTENSION);
        FileOutputStream fos = null;

        savingImageFile(fos, file);
    }
    public void saveColorAndTraceImage(String fileName){
        paintViewBitmap = getBitmapFromView(paintBackgroundView,
                paintBackgroundView.getChildAt(0).getHeight(), paintBackgroundView.getChildAt(0).getWidth());

        File folder = new File(Environment.getExternalStorageDirectory() + PAINTING_FILE_PATH);
        folder.mkdirs();

        File file = new File(folder, fileName + PAINTING_FILE_NAME_EXTENSION);
        FileOutputStream fos = null;

        savingImageFile(fos, file);
    }


    public Bitmap getCacheBitmap(){
        if(paintView!=null){
            paintViewBitmap = getBitmapFromView(paintView , paintView.getCurrentHeight(), paintView.getCurrentWidth());
        } else {
            paintViewBitmap = getBitmapFromView(paintBackgroundView , paintBackgroundView.getChildAt(0).getHeight(), paintBackgroundView.getChildAt(0).getWidth());
        }

        return paintViewBitmap;
    }

    public void savingImageFile(FileOutputStream fos, File file){
        try {
            fos = new FileOutputStream(file);
            paintViewBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            MediaStore.Images.Media.insertImage(context.getContentResolver(), paintViewBitmap, "Screen", "screen");
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArtworkModel> retrieveAllArtworks() {
        File paintingsDirectory = new File(Environment.getExternalStorageDirectory() + PAINTING_FILE_PATH);
        ArrayList<ArtworkModel> artworkModelList = new ArrayList<>();

        if (paintingsDirectory.listFiles() != null){
            for (File file : paintingsDirectory.listFiles()) {
                if (file.isFile())
                    artworkModelList.add(new ArtworkModel(retrieveArtwork(file.getName()),
                            file.getName().substring(0, file.getName().lastIndexOf("."))));
            }
        }

        return artworkModelList;
    }

    public ArrayList<ArtworkModel> retrieveAllSearchedArtworks(String filename) {
        File paintingsDirectory = new File(Environment.getExternalStorageDirectory() + PAINTING_FILE_PATH);
        ArrayList<ArtworkModel> artworkModelList = new ArrayList<>();

        if (paintingsDirectory.listFiles() != null){
            for (File file : paintingsDirectory.listFiles()) {
                if (file.isFile() && file.getName().toLowerCase().startsWith(filename))
                    artworkModelList.add(new ArtworkModel(retrieveArtwork(file.getName()),
                            file.getName().substring(0, file.getName().lastIndexOf("."))));
            }
        }

        return artworkModelList;
    }

    public Bitmap retrieveArtwork(String fileName){
        File artWork = new  File(Environment.getExternalStorageDirectory() + PAINTING_FILE_PATH + "/" + fileName);
        Bitmap artWorkBitmap = null;

        if(artWork.exists()){
            artWorkBitmap = BitmapFactory.decodeFile(artWork.getAbsolutePath());
        }

        return artWorkBitmap;
    }


    public Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth,totalHeight , Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }


}
