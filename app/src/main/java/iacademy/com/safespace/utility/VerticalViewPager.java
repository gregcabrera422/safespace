package iacademy.com.safespace.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

public class VerticalViewPager extends ViewPager {
    private boolean isPagingEnabled = true;

    public VerticalViewPager(Context context) {
        this(context, null);
    }
    public VerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    /**
     * @return {@code false} since a vertical view pager can never be scrolled horizontally
     */
    @Override
    public boolean canScrollHorizontally(int direction) {
        return false;
    }
    /**
     * @return {@code true} iff a normal view pager would support horizontal scrolling at this time
     */
    @Override
    public boolean canScrollVertically(int direction) {
        return super.canScrollHorizontally(direction);
    }
    private void init() {
        // Make page transit vertical
        setPageTransformer(true, new VerticalPageTransformer());
        // Get rid of the overscroll drawing that happens on the left and right (the ripple)
        setOverScrollMode(View.OVER_SCROLL_NEVER);
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //return this.isPagingEnabled && super.onTouchEvent(swapXY(ev));
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //return this.isPagingEnabled && super.onInterceptTouchEvent(swapXY(ev));
        return false;
    }

    private MotionEvent swapXY(MotionEvent ev) {

        //Get display dimensions
        float displayWidth=this.getWidth();
        float displayHeight=this.getHeight();

        //Get current touch position
        float posX=ev.getX();
        float posY=ev.getY();

        //Transform (X,Y) into (Y,X) taking display dimensions into account
        float newPosX=(posY/displayHeight)*displayWidth;
        float newPosY=(1-posX/displayWidth)*displayHeight;

        //swap the x and y coords of the touch event
        ev.setLocation(newPosX, newPosY);

        return ev;
    }
    private static final class VerticalPageTransformer implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View view, float position) {
            final int pageWidth = view.getWidth();
            final int pageHeight = view.getHeight();
            if (position < -1) {
                // This page is way off-screen to the left.
                view.setAlpha(0);
            } else if (position <= 1) {
                view.setAlpha(1);
                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);
                // set Y position to swipe in from top
                float yPosition = position * pageHeight;
                view.setTranslationY(yPosition);
            } else {
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }
}