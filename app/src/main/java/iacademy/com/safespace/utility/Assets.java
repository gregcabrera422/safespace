package iacademy.com.safespace.utility;

import android.graphics.Bitmap;

import java.util.ArrayList;

import iacademy.com.safespace.R;
import iacademy.com.safespace.model.AvatarPartsModel;

public class Assets {

    public static ArrayList<AvatarPartsModel> getEyes(){
        ArrayList<AvatarPartsModel> eyes = new ArrayList<AvatarPartsModel>();
        eyes.add(new AvatarPartsModel(R.drawable.eyes_1_black, 195, 270, "black"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_1_brown, 195, 270, "brown"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_2_black, 197, 270, "black"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_2_brown, 197, 270, "brown"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_3_black, 195, 270, "black"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_3_brown, 195, 270, "brown"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_4_black, 200, 270, "black"));//fix
        eyes.add(new AvatarPartsModel(R.drawable.eyes_4_brown, 200, 270, "brown"));
        eyes.add(new AvatarPartsModel(R.drawable.eyes_5, 200, 270, null));//fix
        eyes.add(new AvatarPartsModel(R.drawable.eyes_6_black, 198, 270, "black"));//fix
        eyes.add(new AvatarPartsModel(R.drawable.eyes_6_brown, 198, 270, "brown"));
        return eyes;
    }

    public static ArrayList<AvatarPartsModel> getHair() {
        ArrayList<AvatarPartsModel> hair = new ArrayList<AvatarPartsModel>();
        hair.add(new AvatarPartsModel(R.drawable.hair_1_black, 170, 220, "black"));
        hair.add(new AvatarPartsModel(R.drawable.hair_1_brown, 170, 220, "brown"));
        hair.add(new AvatarPartsModel(R.drawable.hair_2_black, 170, 220, "black"));
        hair.add(new AvatarPartsModel(R.drawable.hair_2_brown, 170, 220, "brown"));
        hair.add(new AvatarPartsModel(R.drawable.hair_3_black, 170, 220, "black"));//fix
        hair.add(new AvatarPartsModel(R.drawable.hair_3_brown, 170, 220, "brown"));
        hair.add(new AvatarPartsModel(R.drawable.hair_4_black, 155, 231, "black"));//fix
        hair.add(new AvatarPartsModel(R.drawable.hair_4_brown, 155, 231, "brown"));
        hair.add(new AvatarPartsModel(R.drawable.hair_5_black, 170, 220, "black"));//fix
        hair.add(new AvatarPartsModel(R.drawable.hair_5_brown, 170, 220, "brown"));
        hair.add(new AvatarPartsModel(R.drawable.hair_6_black, 135, 220, "black"));//fix
        hair.add(new AvatarPartsModel(R.drawable.hair_6_brown, 135, 220, "brown"));
        return hair;
    }

    public static ArrayList<AvatarPartsModel> getLips() {
        ArrayList<AvatarPartsModel> lips = new ArrayList<AvatarPartsModel>();
        lips.add(new AvatarPartsModel(R.drawable.lips_1, 210, 310, null));
        lips.add(new AvatarPartsModel(R.drawable.lips_2, 210, 310, null));
        lips.add(new AvatarPartsModel(R.drawable.lips_3_pink, 210, 310, "pink"));
        lips.add(new AvatarPartsModel(R.drawable.lips_3_red, 210, 310, "red"));
        lips.add(new AvatarPartsModel(R.drawable.lips_4, 208, 310, null));
        lips.add(new AvatarPartsModel(R.drawable.lips_5, 210, 310, null));
        lips.add(new AvatarPartsModel(R.drawable.lips_6, 208, 310, null));
        return lips;
    }

    public static ArrayList<AvatarPartsModel> getNose() {
        ArrayList<AvatarPartsModel> nose = new ArrayList<AvatarPartsModel>();
        nose.add(new AvatarPartsModel(R.drawable.nose_1, 220, 285, null));
        nose.add(new AvatarPartsModel(R.drawable.nose_2, 220, 285, null));
        nose.add(new AvatarPartsModel(R.drawable.nose_3, 220, 285, null));
        nose.add(new AvatarPartsModel(R.drawable.nose_4, 218, 285, null));
        nose.add(new AvatarPartsModel(R.drawable.nose_5, 220, 280, null));
        nose.add(new AvatarPartsModel(R.drawable.nose_6, 217, 280, null));
        return nose;
    }

    public static ArrayList<AvatarPartsModel> getShirts() {
        ArrayList<AvatarPartsModel> shirts = new ArrayList<AvatarPartsModel>();
        shirts.add(new AvatarPartsModel(R.drawable.shirt_1, 35, 80, null));
        shirts.add(new AvatarPartsModel(R.drawable.shirt_2, 35, 85, null));
        shirts.add(new AvatarPartsModel(R.drawable.shirt_3, 35, 85, null));
        shirts.add(new AvatarPartsModel(R.drawable.shirt_4, 33, 75, null));
        shirts.add(new AvatarPartsModel(R.drawable.shirt_5, 35, 80, null));
        shirts.add(new AvatarPartsModel(R.drawable.shirt_6, 34, 78, null));
        return shirts;
    }

    public static ArrayList<AvatarPartsModel> getSkins() {
        ArrayList<AvatarPartsModel> skins = new ArrayList<AvatarPartsModel>();
        skins.add(new AvatarPartsModel(R.drawable.skin_1, 35, 85, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_2, 35, 85, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_3, 35, 85, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_4, 35, 85, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_5, 35, 85, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_6, 35, 85, null));
        return skins;
    }

    public static ArrayList<AvatarPartsModel> filterSet1(ArrayList<AvatarPartsModel> parts){
        ArrayList<AvatarPartsModel> newArray = new ArrayList<AvatarPartsModel>();
        for (int i = 0; i < parts.size(); ++i){
            if (parts.get(i).getColor() == null || parts.get(i).getColor() == "black" || parts.get(i).getColor() == "pink" ){
                newArray.add(parts.get(i));
            }
        }

        return newArray;
    }

    public static ArrayList<AvatarPartsModel> filterSet2(ArrayList<AvatarPartsModel> parts){
        ArrayList<AvatarPartsModel> newArray = new ArrayList<AvatarPartsModel>();
        for (int i = 0; i < parts.size(); ++i){
            if (parts.get(i).getColor() == "brown" || parts.get(i).getColor() == "red" || parts.get(i).getColor() == null){
                newArray.add(parts.get(i));
            }
        }

        return newArray;
    }

    public static AvatarPartsModel getFrame(){
        return new AvatarPartsModel(R.drawable.avatar_frame, 56, 125, null);
    }

    public static ArrayList<AvatarPartsModel> getDisplaySkin(){
        ArrayList<AvatarPartsModel> skins = new ArrayList<AvatarPartsModel>();
        skins.add(new AvatarPartsModel(R.drawable.skin_1mdpi, 5, 5, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_2mdpi, 5, 5, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_3mdpi, 5, 5, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_4mdpi, 5, 5, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_5mdpi, 5, 5, null));
        skins.add(new AvatarPartsModel(R.drawable.skin_6mdpi, 5, 5, null));
        return skins;
    }

    public static ArrayList<AvatarPartsModel> getDisplayClothes(){
        ArrayList<AvatarPartsModel> clothes = new ArrayList<AvatarPartsModel>();
        clothes.add(new AvatarPartsModel(R.drawable.clothes_2, 0, -5, null));
        clothes.add(new AvatarPartsModel(R.drawable.clothes_1, 0, 5, null));
        clothes.add(new AvatarPartsModel(R.drawable.clothes_3, 0, 0, null));
        clothes.add(new AvatarPartsModel(R.drawable.clothes_4, -10, -20, null));
        clothes.add(new AvatarPartsModel(R.drawable.clothes_5, 0, -5, null));
        clothes.add(new AvatarPartsModel(R.drawable.clothes_6, 0, -10, null));
        return clothes;
    }
}
