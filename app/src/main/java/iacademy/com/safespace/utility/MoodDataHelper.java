package iacademy.com.safespace.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import iacademy.com.safespace.model.MoodModel;

public class MoodDataHelper {

    public static HashMap<String, Integer> getAllMoods(Context context){
        SharedPreferences s = context.getApplicationContext().getSharedPreferences("dates", Context.MODE_PRIVATE);
        HashMap<String, Integer> moods = null;
        if(s!=null) {
            Type type = new TypeToken<List<MoodModel>>() {
            }.getType();
            Gson gson = new Gson();
            Map<String, ?> events = s.getAll();
            moods = new HashMap<>();
            moods.put("sad", 0);
            moods.put("down", 0);
            moods.put("happy", 0);
            moods.put("annoyed", 0);
            moods.put("angry", 0);
            for (Map.Entry<String, ?> event : events.entrySet()) {
                ArrayList<MoodModel> entries = gson.fromJson(event.getValue().toString(), type);
                for (MoodModel entry : entries) {
                    moods.put(entry.getMood(), moods.get(entry.getMood()) + 1);
                }
            }
        }
        return moods;
    }

    public static ArrayList<Integer> getMoodsForLineGraph(Context context){
        SharedPreferences s = context.getApplicationContext().getSharedPreferences("dates", Context.MODE_PRIVATE);
        ArrayList<Integer> moods= null;
        if(s!=null) {
            Type type = new TypeToken<List<MoodModel>>() {
            }.getType();
            Gson gson = new Gson();
            Map<String, ?> events = s.getAll();
            moods = new ArrayList<>();
            for (Map.Entry<String, ?> event : events.entrySet()) {
                ArrayList<MoodModel> entries = gson.fromJson(event.getValue().toString(), type);
                for (MoodModel entry : entries) {
                    switch (entry.getMood()) {
                        case "sad":
                            moods.add(5);
                            break;
                        case "down":
                            moods.add(4);
                            break;
                        case "happy":
                            moods.add(3);
                            break;
                        case "annoyed":
                            moods.add(2);
                            break;
                        case "angry":
                            moods.add(1);
                            break;
                    }
                }
            }
        }
        return moods;
    }
}
