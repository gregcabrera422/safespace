package iacademy.com.safespace.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class SharedPreferencesHelper {
    private final String MY_PREFS_NAME = "Safespace";
    public SharedPreferences getSharedPrefs;

    public SharedPreferencesHelper(Activity activity){
        getSharedPrefs = activity.getApplicationContext().getSharedPreferences(MY_PREFS_NAME,
                        Context.MODE_PRIVATE);
    }

    public void Write(String key, Object value){
        SharedPreferences.Editor editor = getSharedPrefs.edit();

        if(value instanceof String){
            editor.putString(key, value.toString());
        }else if(value instanceof Integer){
            editor.putInt(key, (Integer) value);
        }else if(value instanceof Boolean){
            editor.putBoolean(key, (Boolean) value);
        }else if(value instanceof Float){
            editor.putFloat(key, (Float) value);
        }else if(value instanceof Double){
            editor.putLong(key, Double.doubleToRawLongBits((Double) value));
        }else if(value instanceof Set){
            editor.putStringSet(key, (Set<String>) value);
        }

        //save changes in the background **alternative: editor.commit(); but runs on the main thread**
        editor.apply();
    }
}
